#JNovelClub Reader - A reading application for j-novel.club

Goal
====

The goal of this project is to have a more pleasant reading experience of
J-Novel's novels – on phones, tablets and traditional computers. This includes, but is not limited
to supporting more fonts, arbitrary page width and faster loading times. This is a work in progress,
and not everything might work yet.

Install/Compile/Run
===================

For 64bit Windows, a zip file is provided. Just unzip it, go into the
JNovelReader-64bit directory and run JNovelReader.exe.

For Android, an APK is provided.

For other platforms, no binaries are provided currently. One can however compile
the software onself, provided the following dependencies are met:

- The Qt 5 framework (Qt 5.11 or higher)
- a C++14 compatible compiler (tested with clang++ 5.0.1 and g++ 7.3.0 on Linux,
  as well as MSVC 2017 and mingw-32 5.3).
- Either CMake version 3.10 or qmake from the Qt framework
- Optionally, QtCreator for ease of compilation
- In theory, every platform supported by Qt should work, however only Linux, Android and
  Windows were tested.

Once those dependencies are met, compile the project for Linux/Windows by extracting the source
files and doing the following steps inside the source directory
- mkdir build
- qmake .. (qmake.exe .. on Windows)
- make (nmake on Windows)

- mkdir build
- cmake .. (cmake.exe .. on Windows)
- make (nmake on Windows)

Alternatively, open JNovelReader.pro in QtCreator and let it work its magic.

Now you can run JNovelReader

How to use it
=============

When starting JNovelReader, it displays a list of all volumes as published by
j-novel.club. Before continuing, if you are a site member, click on the
hamburger menu in the right bottom corner. This opens a drawer, where you can
enter your email address and password, to log in. ![login](https://i.cubeupload.com/MM8rcS.jpg)
Without doing so, access to most series but Rokujoma and 
“The Magic in this Other World is Too Far Behind!” is limited to the first part
of each volume.

Note that JNovelReader does not store your password, but only an access token,
which expires after two weeks, so you'll have to log in again afterwards.

Once you've logged in, you can click on any series name in the left scrollview.
To reduce the number of entries, you can use the upper bar to filter the series
![filtering](http://i.cubeupload.com/kbrQmz.jpg)
Afterwards, click on the “Show Volumes” button. This opens a new screen, which
displays all volumes of the series. After selecting a volume, you can select
which part you want to read. By clicking on the corresponding button, the part
gets opened (if you cannot click on the part and there is a lock symbol, this
means that the part is either expired, or you are not logged in).

Once you've opened the part, you can navigate in the story or change the
settings for font and color scheme.

To navigate, click on the "<" and ">" buttons to go to the previous and next
page. Click on the |< and |> buttons to skip to the previous and next part. You
can use the dropdown displaying the current page number/total page count to jump
to a specific page.

Clicking on "Change font" allows you to select any font installed on your
system, in any size you might want (even Comic Sans MS if you are so inclined).

The checkboxes allow you to switch between a light, dark and sepia colour
scheme.

![dark scheme](https://i.cubeupload.com/4qXiqK.jpg)
![light scheme](https://i.cubeupload.com/b09c20.jpg)

In case you want to select a different part, volume or series, you can click on
the <== button to go back to the previous screen until you arrive at the initial
screen.


There is also a clock button on the inital view. This opens a drawer showing the
latest released parts and allows you to directly open them.

Known Limitations/Bugs
======================

Keyboard navigation is extremely flaky. You cannot navigate the listview at all
with keys, and you can only change the parts in the part viewer once you've
clicked on a button.

While the applications saves your reading progress and sends it to the
j-novel.club server, it currently doesn't jump to the last page you were on.

Once you've jumped to a part from the latest release drawer, it is impossible to
directly jump to a previous part of the series. You have to go back to the
volume overview.

The application is mostly tested on Linux, so there might occur some crashes on
Windows. If this is the case, please report what you've done before starting the
application.


License
=======

This project is licensed under the 3 clause BSD license.

This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit
(http://www.openssl.org/) (see OpenSSL-license.txt)

This project uses the Qt framework under its LGPLv3 license. To obtain the
source code, just send an email to the author of JNovelReader or ask in the
forum (see  Qt-lgpl3.txt).

This project uses the IcoMoon font under its CC BY 4.0 license
(see https://creativecommons.org/licenses/by/4.0/legalcode)
