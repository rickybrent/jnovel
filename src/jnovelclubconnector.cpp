#include "jnovelclubconnector.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <QUrlQuery>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QSettings>
#include <QDateTime>
#include <QHostInfo>
#include <QSslConfiguration>
#include <QTimer>
#include <QNetworkCookie>

#include <algorithm>

#include "parts.h"
#include "json_helper.h"


JNovelClubConnector::JNovelClubConnector(QNetworkAccessManager *manager, QObject *parent)  :
    QObject(parent),
    m_manager(manager),
    m_settings(new QSettings(QStringLiteral("J-novel.club"),
                             QStringLiteral("reader"), this)),
    m_currentSeriesID(QLatin1String("")),
    m_accessToken(QLatin1String("")),
    m_userid(m_settings->value(QStringLiteral("userID"), "").toString()),
    m_lastFetchedPart({})
{
    QHostInfo::lookupHost(QStringLiteral("api.j-novel.club"), [](auto info){(void) info;});
    // ensure that HTTP/2 is used when possible
    auto sslConfig = QSslConfiguration::defaultConfiguration();
    auto allowed = sslConfig.allowedNextProtocols();
    allowed.push_front(QSslConfiguration::ALPNProtocolHTTP2);
    sslConfig.setAllowedNextProtocols(allowed);
    manager->connectToHostEncrypted(QStringLiteral("api.j-novel.club"), 443);
}

void JNovelClubConnector::handleNetworkError(QNetworkReply* reply) {
    qDebug() << reply->errorString();
}

void JNovelClubConnector::fetchLNSeriesList()
{
    emit LNSeriesFetched(SeriesList {});
    QUrl getAllSeriesURL(QStringLiteral("https://api.j-novel.club/api/series"));
    QNetworkRequest request(getAllSeriesURL);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);

    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto seriesListJSON = parsedJSON.array();
        SeriesList seriesList = SeriesList();
        seriesList.reserve(seriesListJSON.size());
        std::transform(seriesListJSON.constBegin(), seriesListJSON.constEnd(), std::back_inserter(seriesList), [](const auto& seriesGeneric) {
            auto series = seriesGeneric.toObject();
            return Series::fromJSON(series);
        });
        emit LNSeriesFetched(seriesList);
    });
}

void JNovelClubConnector::fetchMangaSeriesList()
{
    emit MangaSeriesFetched(SeriesList {});
    QUrl getAllSeriesURL(QStringLiteral("https://api.j-novel.club/api/mangaseries"));
    QNetworkRequest request(getAllSeriesURL);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);

    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto seriesListJSON = parsedJSON.array();
        SeriesList seriesList = SeriesList();
        seriesList.reserve(seriesListJSON.size());
        std::transform(seriesListJSON.constBegin(), seriesListJSON.constEnd(), std::back_inserter(seriesList), [](const auto& seriesGeneric) {
            auto series = seriesGeneric.toObject();
            return Series::fromJSON(series, BookType::Manga);
        });
        emit MangaSeriesFetched(seriesList);
    });
}

void JNovelClubConnector::refreshVolumes()
{
    fetchLNVolumesForID(m_currentSeriesID);
}

void JNovelClubConnector::fetchLNVolumesForID(const QString& seriesID)
{
    m_currentSeriesID = seriesID;
    auto queryString = QStringLiteral("https://api.j-novel.club/api/volumes?filter[where][serieId]=%1&filter[include][parts]").arg(seriesID);
    QUrl getVolumesWithParts(queryString);
    QNetworkRequest request(getVolumesWithParts);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto volumesJSON = parsedJSON.array();
        VolumeList volumes {};
        volumes.reserve(volumesJSON.size());
        for (const auto volumeGeneric: volumesJSON) {
            auto volume = volumeGeneric.toObject();
            volumes.push_back(Volume::fromJSON(volume));
        }
        emit LNVolumesFetched(volumes);
    });
}

void JNovelClubConnector::fetchMangaVolumesForID(const QString& seriesID)
{
    m_currentSeriesID = seriesID;
    auto queryString = QStringLiteral("https://api.j-novel.club/api/mangaVolumes?filter[where][mangaSerieId]=%1&filter[include][mangaParts]").arg(seriesID);
    QUrl getVolumesWithParts(queryString);
    QNetworkRequest request(getVolumesWithParts);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJSON = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJSON.isArray());
        auto volumesJSON = parsedJSON.array();
        VolumeList volumes {};
        volumes.reserve(volumesJSON.size());
        for (const auto volumeGeneric: volumesJSON) {
            auto volume = volumeGeneric.toObject();
            volumes.push_back(Volume::fromJSON(volume, BookType::Manga));
        }
        emit MangaVolumesFetched(volumes);
    });
}

void JNovelClubConnector::fetchPart(const Part part, qreal percentageRead)
{
    // https://j-novel.club/mc/the-master-of-ragnarok-blesser-of-einherjar-vol-1-manga     https://j-novel.club/mc/the-master-of-ragnarok-blesser-of-einherjar-vol-1-ch-2
    const auto queryString = QStringLiteral("https://api.j-novel.club/api/parts/%1/partData").arg(part.partID());
    m_lastFetchedPart = part;
    emit lastFetchedPartChanged();
    QUrl getPart(queryString);
    QNetworkRequest request(getPart);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute,
                         QNetworkRequest::PreferCache);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply, percentageRead]{
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);
        Q_ASSERT(parsedJson.isObject());
        auto partText = parsedJson.object()[QStringLiteral("dataHTML")].toString();
        emit partFetched(partText, percentageRead);
    });
}

bool JNovelClubConnector::checkIfLoggedIn()
{
    if (loggedIn()) {
        return true;
    }
    auto variant = m_settings->value(QStringLiteral("expirationDate"));
    if (variant.isValid() && QDateTime::fromString(variant.toString(), Qt::ISODateWithMs) > QDateTime::currentDateTime()) {
        m_accessToken = m_settings->value(QStringLiteral("accessToken")).toString();
        m_loginStatus = LoggedIn;
        emit loginSuccess();
        emit loggedInChanged();
        emit loginStatusChanged(m_loginStatus);
        // renew one day before it lapses
        auto renewalTime = qMin(0ll, QDateTime::currentDateTime().msecsTo(QDateTime::fromString(variant.toString(), Qt::ISODateWithMs).addDays(-1)));
        QTimer::singleShot(renewalTime, this, &JNovelClubConnector::loginWithStoredCredentials);
        return true;
    } else {
        m_settings->remove(QStringLiteral("accessToken"));
        m_settings->remove(QStringLiteral("expirationDate"));
        m_loginStatus = LoggedOff;
        emit loginStatusChanged(m_loginStatus);
        emit loggedInChanged();
        loginWithStoredCredentials();
    }
    return false;
}

QString JNovelClubConnector::createAccessCookiesString()
{
    auto const expirationDate = QDateTime::fromString(m_settings->value(QStringLiteral("expirationDate")).toString(), Qt::ISODateWithMs);
    auto accessCookie = QNetworkCookie { "access_token", m_settings->value(QStringLiteral("accessTokenValue")).toByteArray()};
    accessCookie.setDomain(QLatin1String(".j-novel.club"));
    accessCookie.setPath(QLatin1String("/"));
    accessCookie.setExpirationDate(expirationDate);
    auto userCookie = QNetworkCookie { "userId", m_settings->value(QStringLiteral("userIdValue")).toByteArray()};
    userCookie.setDomain(QLatin1String(".j-novel.club"));
    userCookie.setPath(QLatin1String("/"));
    userCookie.setExpirationDate(expirationDate);
    QString ret {accessCookie.toRawForm()};
    return ret + userCookie.toRawForm();
}

void JNovelClubConnector::fetchUserData()
{
    const auto queryString = QStringLiteral("https://api.j-novel.club/api/users/%1?filter={\"include\":[{\"ownedBooks\":\"serie\"},\"readParts\",\"roles\"]}").arg(m_userid);
    QUrl getUserData(queryString);
    QNetworkRequest request(getUserData);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply]{
        reply->deleteLater();
        if (reply->error()) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);
        auto user = User::fromJSON(parsedJson.object());
        emit fetchedUserData(user);
    });
}

void JNovelClubConnector::loginWithStoredCredentials()
{
    auto email = m_settings->value(QStringLiteral("emailAddress")).toString();
    auto password = m_settings->value(QStringLiteral("password")).toString();
    if (! (email.isEmpty() || password.isEmpty() ) )
        login(email, password);
}

void JNovelClubConnector::login(const QString& email, const QString& pass)
{
    m_loginStatus = Pending;
    emit loginStatusChanged(m_loginStatus);
    auto queryString = QStringLiteral("https://api.j-novel.club/api/users/login?include=user");
    QUrl logMeIn(queryString);
    QNetworkRequest request(logMeIn);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject payload;
    payload.insert(QStringLiteral("email"), email);
    payload.insert(QStringLiteral("password"), pass);

    QJsonDocument document;
    document.setObject(payload);


    QNetworkReply* reply = m_manager->post(request, document.toJson(QJsonDocument::Compact));


    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply](){
        reply->deleteLater();
        switch (reply->error()) {
            case QNetworkReply::NoError:
                break;
            default:
                m_loginStatus = Failed;
                emit loginStatusChanged(m_loginStatus);
                emit loginFailed(QStringLiteral("Login failed!"));
        }
        QVariant cookieVariant = reply->header(QNetworkRequest::SetCookieHeader);
        if (cookieVariant.isValid()) {
            auto cookies = cookieVariant.value<QList<QNetworkCookie>>();
            const auto authCookieIt = std::find_if(cookies.constBegin(), cookies.constEnd(), [](const auto& cookie){
                return cookie.name() == "access_token";
            });
            const auto userCookieIt = std::find_if(cookies.constBegin(), cookies.constEnd(), [](const auto& cookie){
                return cookie.name() == "userId";
            });
            if (authCookieIt != cookies.constEnd()) {
                const auto& authCookie = *authCookieIt;
                const auto& userCookie = (userCookieIt != cookies.constEnd()) ? *userCookieIt : *authCookieIt; // use some default value for the unlikely case that the cookie does not exist
                auto expiresOn = authCookie.expirationDate();
                auto data = reply->readAll();
                auto json = QJsonDocument::fromJson( data);
                m_userid = json.object()[QStringLiteral("userId")].toString();
                m_accessToken = QString::fromUtf8(authCookie.value()).mid(4, 64);
                m_settings->setValue(QStringLiteral("expirationDate"), expiresOn.toString(Qt::ISODateWithMs));
                m_settings->setValue(QStringLiteral("accessToken"), m_accessToken);
                m_settings->setValue(QStringLiteral("userIdValue"), userCookie.value());
                m_settings->setValue(QStringLiteral("accessTokenValue"), authCookie.value());
                m_settings->setValue(QStringLiteral("userID"), m_userid);
                m_loginStatus = LoggedIn;
                emit loginStatusChanged(m_loginStatus);
                emit loginSuccess();
                emit loggedInChanged();
            }
        }
    });
}

void JNovelClubConnector::updatePartCompletionStatus(const Part& part, qreal readPercentage)
{
    if (!loggedIn()) {
        return;
    }
    auto queryString = QStringLiteral("https://api.j-novel.club/api/users/%1/updateReadCompletion");
    QUrl updateReadCompletion(queryString.arg(m_userid));
    QNetworkRequest request(updateReadCompletion);
    request.setRawHeader("authorization", m_accessToken.toLocal8Bit()); // Qt doesn't handle bearer authentification
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject payload;
    payload.insert(QStringLiteral("partId"), part.partID());
    payload.insert(QStringLiteral("completion"), readPercentage);

    QJsonDocument document;
    document.setObject(payload);


    QNetworkReply* reply = m_manager->post(request, document.toJson(QJsonDocument::Compact));

    QObject::connect(reply, &QNetworkReply::finished, reply, [reply](){
        reply->deleteLater();
    });
}

void JNovelClubConnector::setAccessToken(const QString &accessToken)
{
    m_accessToken = accessToken;
    emit loggedInChanged();
}

bool JNovelClubConnector::loggedIn()
{
    return !m_accessToken.isEmpty();
}

int JNovelClubConnector::loginStatus() const
{
    return m_loginStatus;
}

void JNovelClubConnector::fetchLatestReleasedParts(int offset, unsigned limit)
{
    // TODO: there's still a race condition when the network reply for a later request arrives before the finish of a previous one
    if (offset == -1)
        offset = m_alreadyFetchedCount;
    // hack to reset model
    bool resetModel = false;
    if (offset == -2 ) {
        offset = 0;
        m_alreadyFetchedCount = 0;
        resetModel = true;
    }
    m_alreadyFetchedCount += limit;
    auto queryString = QStringLiteral ("https://api.j-novel.club/api/parts");
    QUrl requsetLatestParts(queryString);
    // ?filter[order]=launchDate%%20DESC&filter[limit]=%d&filter[offset]=%d
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("filter[order][0]"), QStringLiteral("launchDate DESC"));
    query.addQueryItem(QStringLiteral("filter[order][1]"), QStringLiteral("title DESC"));
    query.addQueryItem(QStringLiteral("filter[offset]"), QString::number(offset));
    query.addQueryItem(QStringLiteral("filter[limit]"), QString::number(limit));
    requsetLatestParts.setQuery(query);

    QNetworkRequest request(requsetLatestParts);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QNetworkReply* reply = m_manager->get(request);
    QObject::connect(reply, &QNetworkReply::finished, reply, [this, reply, resetModel](){
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            this->handleNetworkError(reply);
            return;
        }
        auto data = reply->readAll();
        auto parsedJson = QJsonDocument::fromJson(data);

        Q_ASSERT(parsedJson.isArray());
        auto partsArray = parsedJson.array();
        PartList parts {};
        parts.reserve(partsArray.size());
        for (const auto partGeneric: partsArray) {
            parts.push_back(Part::fromJSON(partGeneric.toObject(), BookType::LightNovel));
        }
        if (resetModel)
            emit resetLatestReleasedParts(parts);
        else
            emit fetchedLatestReleasedParts(parts);
    });
}


