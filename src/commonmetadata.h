#ifndef COMMONMETADATA_H
#define COMMONMETADATA_H

#include <QString>
#include <QUrl>
#include <QStringList>

#include <boost/flyweight.hpp>

#include "booktype.h"


// provide hash function for Boost Flyweight
inline std::size_t hash_value(QString const& s)
{
    return qHash(s);
}

// tag types for flyweight
struct authorT {};
struct illustratorOrArtistT {};
struct translatorT {};
struct editorT {};

// List of meta data
#define CommonMetadataList \
    X(QString, title)\
    X(QString, titleslug)\
    X(QString, author)\
    X(QString, illustratorOrArtist)\
    X(QString, translator)\
    X(QString, editor)\
    X(QString, description)\
    X(QStringList, tags)\
    X(QUrl, imageUrl)\
    X(QString, descriptionShort)\
    X(QUrl, previewImageUrl)


class CommonMetadata
{
public:
    CommonMetadata(const QString& title, const QString& titleslug, const QString& titleShort, const QString& titleOriginal,
                   const QString& author, const QString& illustrator, const QString& translator, const QString& editor,
                   const QString& description, const QString& descriptionShort, const QString& tags, const QString& imageUrlFragment,
                   const QString& previewImageUrlFragment)
        : m_title(title), m_titleShort(titleShort), m_titleOriginal(titleOriginal), m_titleslug(titleslug), m_author(author), m_illustratorOrArtist(illustrator), m_translator(translator),
          m_editor(editor), m_description(description), m_descriptionShort(descriptionShort),
          m_tags(tags.split(QStringLiteral(", "))),
          m_previewImageUrl(QStringLiteral("https://d2dq7ifhe7bu0f.cloudfront.net/") + previewImageUrlFragment),
          m_imageUrl(QStringLiteral("https://d2dq7ifhe7bu0f.cloudfront.net/") + imageUrlFragment) {}

#define X(type, name) type name() const;
    CommonMetadataList
#undef X

private:
    template<typename X>
    using tag = boost::flyweights::tag<X>;
    QString m_title;
    QString m_titleShort;
    QString m_titleOriginal;
    QString m_titleslug;
    boost::flyweights::flyweight<QString, tag<authorT>> m_author;
    boost::flyweights::flyweight<QString, tag<illustratorOrArtistT>> m_illustratorOrArtist;
    boost::flyweights::flyweight<QString, tag<translatorT>> m_translator;
    boost::flyweights::flyweight<QString, tag<editorT>> m_editor;
    QString m_description;
    QString m_descriptionShort;
    QStringList m_tags;
    QUrl m_previewImageUrl;
    QUrl m_imageUrl;
};

Q_DECLARE_TYPEINFO(CommonMetadata, Q_MOVABLE_TYPE);


#endif // COMMONMETADATA_H
