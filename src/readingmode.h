#ifndef READINGMODE_H
#define READINGMODE_H

#include <qobjectdefs.h>
#include <QColor>

namespace Reading
{    
    Q_NAMESPACE
    
    enum class ColorScheme { BLACK=3, LIGHT=2, DARK=1, SEPIA=0 };
    Q_ENUM_NS(ColorScheme)
    
    QColor mode2bgColor(ColorScheme mode);
    QColor mode2fgColor(ColorScheme mode);
}


#endif
