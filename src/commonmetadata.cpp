#include "commonmetadata.h"

#define X(type, name) type CommonMetadata::name() const { \
    return m_ ## name; \
    }
CommonMetadataList
#undef X
