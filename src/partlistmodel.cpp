#include <algorithm>

#include "partlistmodel.h"
#include "parts.h"
#include "volume.h"
#include "volumelistmodel.h"

PartListModel::PartListModel(QObject *parent)
    : QAbstractListModel(parent), m_parts( {})
{
}

int PartListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return static_cast<int>(m_parts.size());
}

QVariant PartListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (m_parts.empty() || static_cast<int>(m_parts.size()) <= index.row())
        return QVariant();

    const auto& part = m_parts.at(static_cast<decltype (m_parts.size())>(index.row()));
    switch (role) {
    case PartIDRole:
        return part.partID();
    case TitleRole:
        return part.title();
    case TitleSlugRole:
        return part.titleSlug();
    case ExpiredRole:
        return part.expired();
    case PreviewRole:
        return part.preview();
    case ThumbnailRole:
        return part.thumbnailPath();
    case AdapterRole:
        return QVariant::fromValue(part);
    default:
        return QVariant();
    }
}

void PartListModel::setParts(const PartList& parts)
{
    beginResetModel();
    m_parts = parts;
    endResetModel();
}

QHash<int, QByteArray> PartListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PartIDRole] = "partID";
    roles[ExpiredRole] = "isExpired";
    roles[PreviewRole] = "isPreview";
    roles[ThumbnailRole] = "thumbnail";
    roles[AdapterRole] = "adapter";
    roles[TitleRole] = "title";
    roles[TitleSlugRole] = "titleSlug";
    return roles;
}


void PartListModel::addMoreParts(const PartList &newParts)
{
    const QModelIndex parent {};
    auto row = static_cast<int>(m_parts.size());
    auto count = static_cast<int>(newParts.size());
    beginInsertRows(parent, row, row + count - 1);
    m_parts.insert(m_parts.end(), newParts.begin(), newParts.end());
    endInsertRows();
}
