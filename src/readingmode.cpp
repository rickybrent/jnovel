#include "readingmode.h"

#include <QColor>

QColor Reading::mode2bgColor(Reading::ColorScheme mode)
{
    using namespace Reading;
    switch (mode) {
        case ColorScheme::BLACK:
            return QColor(0, 0, 0);
        case ColorScheme::DARK:
            return QColor(51, 51, 51);
        case ColorScheme::LIGHT:
            return QColor(255, 255, 255);
        case ColorScheme::SEPIA:
            return QColor(244, 236, 216);
        default:
            return {};
    }
}

QColor Reading::mode2fgColor(Reading::ColorScheme mode)
{
    using namespace Reading;
    switch (mode) {
        case ColorScheme::BLACK:
            return QColor(252,252,252);
        case ColorScheme::DARK:
            return QColor(238,238,238);
        case ColorScheme::LIGHT:
            return QColor(51,51,51);
        case ColorScheme::SEPIA:
            return QColor(91, 70, 54);
        default:
            return {};
    }
}
