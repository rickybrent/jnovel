#ifndef SERIESLIST_H
#define SERIESLIST_H

#include "series.h"
#include <boost/container/vector.hpp>

using SeriesList = boost::container::vector<Series>;


#endif // SERIESLIST_H
