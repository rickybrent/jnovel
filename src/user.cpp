#include "user.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

User::User()
{

}

User User::fromJSON(const QJsonObject& json)
{

    User user {};
    user.name = json.value(QStringLiteral("username")).toString();
    user.email = json.value(QStringLiteral("email")).toString();
    user.id = json.value(QStringLiteral("id")).toString();
    QJsonArray parts = json.value(QStringLiteral("readParts")).toArray();
    user.part2completionStatus.reserve(parts.size());
    for (QJsonArray::const_iterator it = parts.constBegin(); it != parts.constEnd(); ++it) {
        bool b = (*it).isObject();
        (void) b;
        QJsonObject part = it->toObject();
        CompletionStatus completion {};
        completion.maxCompletion = part[QStringLiteral("maxCompletion")].toDouble();
        completion.completion = part[QStringLiteral("completion")].toDouble();
        user.part2completionStatus[part[QStringLiteral("partId")].toString()] = completion;
    }
    QJsonArray ownedBooks = json.value(QStringLiteral("ownedBooks")).toArray();
    user.library.reserve(ownedBooks.size());
    for (QJsonArray::const_iterator it = ownedBooks.constBegin();  it != ownedBooks.constEnd(); ++it) {
        user.library.push_back(Volume::fromJSON(it->toObject()));
    }

    return user;
}

QString User::getId() const
{
    return id;
}

QString User::getName() const
{
    return name;
}

QString User::getEmail() const
{
    return email;
}

VolumeList User::getLibrary() const
{
    return library;
}

QHash<QString, CompletionStatus> User::getPart2completionStatus() const
{
    return part2completionStatus;
}

void User::setPartCompletion(const QString &partID, qreal percentRead)
{
    auto& completionStatus = part2completionStatus[partID];
    completionStatus.completion = percentRead;
    if (completionStatus.maxCompletion < percentRead) {
        completionStatus.maxCompletion = percentRead;
    }
}
