#ifndef SERIESLISTMODEL_H
#define SERIESLISTMODEL_H

#include <QAbstractListModel>
#include <QHash>
#include <QByteArray>
#include "series.h"
#include "serieslist.h"

class SeriesListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(SeriesListModel)

public:
    explicit SeriesListModel(QObject *parent = nullptr);

    enum SerieslRoles {
        TitleRole = Qt::UserRole + 1,
        AuthorRole,
        IllustratorOrArtistRole,
        TranslatorRole,
        EditorRole,
        DescriptionRole,
        TagsRole,
        DescriptionShortRole,
        ImageURLRole,
        PreviewImageURLRole,
        SeriesIDRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
    void addSeries(const Series &series);
    void setSeriesList(const SeriesList &series);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    SeriesList m_seriesList = {};
};

#endif // SERIESLISTMODEL_H
