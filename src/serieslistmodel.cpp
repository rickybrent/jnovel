#include "serieslistmodel.h"

SeriesListModel::SeriesListModel (QObject* parent ) : QAbstractListModel(parent)
{

}

int SeriesListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return static_cast<int>(m_seriesList.size());
}

QHash<int, QByteArray> SeriesListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[TitleRole] = "title";
    roles[AuthorRole] = "author";
    roles[IllustratorOrArtistRole] = "illustratorOrArtist";
    roles[TranslatorRole] = "translator";
    roles[EditorRole] = "editor";
    roles[DescriptionRole] = "description";
    roles[TagsRole] = "tags";
    roles[DescriptionShortRole] = "shortDescription";
    roles[ImageURLRole] = "imageURL";
    roles[PreviewImageURLRole] = "previewImageURL";
    roles[SeriesIDRole] = "seriesID";
    return roles;
}

QVariant SeriesListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() < 0 || static_cast<decltype(m_seriesList.size())>(index.row()) >= m_seriesList.size())
        return QVariant();

    const auto& series = m_seriesList.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
        //fallthrough
    case TitleRole:
        return series.title();
    case AuthorRole:
        return series.author();
    case IllustratorOrArtistRole:
        return series.illustratorOrArtist();
    case TranslatorRole:
        return series.translator();
    case EditorRole:
        return series.editor();
    case DescriptionRole:
        return series.description();
    case TagsRole:
        return series.tags();
    case ImageURLRole:
        return series.imageUrl();
    case PreviewImageURLRole:
        return series.previewImageUrl();
    case SeriesIDRole:
        return series.seriesID();
    default:
       return QVariant();
    }
}

void SeriesListModel::addSeries(const Series& series)
{
    const QModelIndex parent {};
    auto row = static_cast<int>(m_seriesList.size());
    int count = 1;
    beginInsertRows(parent, row, row + count - 1);
    m_seriesList.push_back(series);
    endInsertRows();
}

void SeriesListModel::setSeriesList(const SeriesList &series)
{
    const QModelIndex parent {};
    m_seriesList.clear();
    beginResetModel();
    m_seriesList.insert(m_seriesList.end(), series.begin(), series.end());
    endResetModel();
}
