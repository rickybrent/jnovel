#include <QJsonObject>

#include "parts.h"
#include "json_helper.h"


Part::Part(const QString& partID, const QString& title, const QString& titleSlug,  const QString& volumeID, const QString& seriesID, const QString& thumbnail, bool isPreview, bool isExpired)
: m_partID(partID), m_title(title), m_titleSlug(titleSlug), m_volumeID(volumeID), m_seriesID(seriesID), m_thumbnailPath(QStringLiteral("https://s3.amazonaws.com/production.j-novel.club/") +  thumbnail), m_preview(isPreview), m_expired(isExpired) {}

Part::Part() : Part(QStringLiteral(""), QStringLiteral("invalid"), QStringLiteral(""), QStringLiteral(""), QStringLiteral(""), QStringLiteral(""), false, false) {}

QString Part::partID() const {return m_partID;}

QString Part::volumeID() const {return m_volumeID;}

QString Part::seriesID() const {return m_seriesID;}

QString Part::title() const {return m_title;}

QString Part::titleSlug() const {return m_titleSlug;}

QString Part::thumbnailPath() const {return m_thumbnailPath;}

Part Part::fromJSON(const QJsonObject &json, BookType type)
{
    return Part  {
        json[QStringLiteral("id")].toString(),
        json[type == BookType::LightNovel ? QStringLiteral("title") : QStringLiteral("partTitle")].toString(),
        json[QStringLiteral("titleslug")].toString(),
        json[QStringLiteral("volumeId")].toString(),
        json[QStringLiteral("serieId"/*serie, not series*/)].toString(),
        partJson2thumbnailPath(json),
        json[QStringLiteral("preview")].toBool(),
        json[QStringLiteral("expired")].toBool()
    };
}

bool Part::preview() const {return m_preview;}

bool Part::expired() const {return m_expired;}

bool operator==(const Part &lhs, const Part &rhs) {return lhs.partID() == rhs.partID();}

bool operator!=(const Part &lhs, const Part &rhs) {return !(lhs == rhs);}
