#ifndef PAGEVIEW_H
#define PAGEVIEW_H

#include <QQuickPaintedItem>
#include <QImage>
#include <QHash>
#include <QFont>
#include <QPixmap>
#include <QTextDocument>
#include <array>
#include <QMutex>

#include "readingmode.h"

class QPainter;
class QScreen;
class QSettings;
class QPropertyAnimation;

using PageCache = std::array<QImage, 6>;

using CacheEntry = PageCache::size_type;


class PageView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_DISABLE_COPY(PageView)

public:
    Q_PROPERTY(int pageNumber READ pageNumber WRITE setPageNumber NOTIFY pageNumberChanged)
    Q_PROPERTY(int pageCount READ pageCount NOTIFY pageCountChanged)
    Q_PROPERTY(bool useTwoPages READ useTwoPages WRITE setuseTwoPages NOTIFY useTwoPagesChanged)
    Q_PROPERTY(int colorScheme READ colorScheme NOTIFY colorSchemeChanged)
    Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(qreal pageOffset READ pageOffset WRITE setPageOffset NOTIFY pageOffsetChanged)

public:
    PageView(QQuickItem *parent = nullptr);
    void paint(QPainter *painter) override;

    int pageNumber() const;
    void setPageNumber(int pageNumber);
    int pageCount() const;
    bool useTwoPages() const;
    void setuseTwoPages(bool useTwoPages);
    void setFont(const QFont& newFont);
    QFont font();
    Q_INVOKABLE void resetPageOffset();
    Q_INVOKABLE void keepScreenOn(bool enabled);

    qreal pageOffset() const;

signals:
    void parsingStarted();
    void parsingEnded();
    void pageNumberChanged();
    void pageCountChanged();
    void useTwoPagesChanged();
    void documentPreparationComplete(void* ptr);
    void colorSchemeChanged();
    void fontChanged();
    void percentReadChanged(qreal percentage);
    void pageOffsetChanged(qreal offset); // actually never emitted as there is noone listening to it

public slots:
    void setText(const QString& part, qreal percentage);
    void setColorScheme(Reading::ColorScheme readingMode);
    // stupid overload of setColorScheme because QML doesn't manage to pass the enum class to the function
    void setColorScheme(int colorScheme) {setColorScheme(static_cast<Reading::ColorScheme>(colorScheme));}

    void setPageOffset(qreal pageOffset);
    void handleClick(double x, double y);

private:

    QHash<QString, QImage> m_URL2originalImage;
    QTextDocument m_textDocument;
    QMutex m_documentMutex;
    QPropertyAnimation* m_offsetAnimation;
    QFont m_font;
    PageCache m_pageCache;
    qreal m_pageOffset;
    int m_pageNumber;
    int m_pageCount;
    Reading::ColorScheme m_currentColorScheme = Reading::ColorScheme::DARK;
    bool m_useTwoPages = true;

    int colorScheme();
    qreal pageWidth() const;
    void handleResize();
    void setupPainter(QPainter* painter);
    void renderPage(QPainter* painter, int pageNumber);
    void populateCache();
    void asyncPopulateCache();
    void cacheForward(int dist);
    void cacheBackward(int dist);
    void addCacheEntry(const CacheEntry entry, const int page);
    void drawPageSeparator(QPainter* painter, double initialX);
    void asyncPopuateHelper(void);
    bool doesPageContainImportantPicture(int pageNumber);
    QPointF translatePageViewCoordinates2DocumentCoordinates(qreal x, qreal y);
};

#endif // PAGEVIEW_H
