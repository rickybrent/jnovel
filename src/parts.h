#ifndef PARTS_H
#define PARTS_H

#include <QString>
#include <QObject>
#include "booktype.h"

class Part
{
    Q_GADGET

    Q_PROPERTY(QString partID MEMBER m_partID READ partID)
    Q_PROPERTY(QString volumeID MEMBER m_volumeID READ volumeID)
    Q_PROPERTY(QString seriesID MEMBER m_seriesID READ seriesID)
    Q_PROPERTY(QString title MEMBER m_title READ title)
    Q_PROPERTY(QString titleSlug MEMBER m_titleSlug READ titleSlug)


public:
    Part(const QString& pardID, const QString& title, const QString& titleSlug, const QString& volumeID, const QString& seriesID, const QString& thumbnail, bool isPreview, bool isExpired);
    Part();

private:
    QString m_partID;
    QString m_title;
    QString m_titleSlug;
    QString m_volumeID;
    QString m_seriesID;
    QString m_thumbnailPath;
    bool m_preview;
    bool m_expired;

public:
    QString partID() const;
    QString volumeID() const;
    QString seriesID() const;
    QString title() const;
    QString titleSlug() const;
    QString thumbnailPath() const;
    bool preview() const;
    bool expired() const;

    static Part fromJSON(const QJsonObject& json, BookType type);
};

bool operator==(const Part& lhs, const Part& rhs);
bool operator!=(const Part& lhs, const Part& rhs);

Q_DECLARE_TYPEINFO(Part, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(Part)

#endif // PARTS_H
