#ifndef SERIES_H
#define SERIES_H

#include <QString>
#include <QStringList>

class QJsonObject;

#include "commonmetadata.h"

class Series
{
public:
    Series(const QString& title, const QString& titleslug, const QString& titleShort, const QString& titleOriginal, const QString& author,
           const QString& illustrator, const QString& translator, const QString & editor,
           const QString& description, const QString& descriptionShort, const QString& tags, const QString& imageUrlFragment,
           const QString& previewImageUrlFragment, const QString&  seriesID)
        : m_metaData(title, titleslug, titleShort, titleOriginal, author, illustrator, translator, editor, description,
                         descriptionShort, tags, imageUrlFragment, previewImageUrlFragment), m_seriesID(seriesID) {}

    static Series fromJSON(const QJsonObject& series, const BookType type = BookType::LightNovel);

    QString seriesID() const;
#define X(type, name) type name() const;
    CommonMetadataList
#undef X

private:
    CommonMetadata m_metaData;
    QString m_seriesID;
};

Q_DECLARE_TYPEINFO(Series, Q_MOVABLE_TYPE);

#endif // SERIES_H
