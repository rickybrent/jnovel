#include <QPainter>
#include <QTextDocument>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include <QNetworkAccessManager>
#include <QQmlEngine>
#include <QQmlContext>
#include <QNetworkReply>
#include <QAbstractTextDocumentLayout>
#include <QPropertyAnimation>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QMetaEnum>
#include <QFuture>
#include <QtConcurrent>
#include <QMutexLocker>
#include <QtGlobal>
#include <QGuiApplication>
#include <QTextBlock>
#include <QTextCursor>
#include <QMouseEvent>

#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#endif

#include <boost/container/small_vector.hpp>

#include <3rdparty/asyncfuture.h>

#include <cmath>
#include <algorithm>
#include <memory>

#include "pageview.h"

constexpr CacheEntry prev_prev_page = 0;
constexpr CacheEntry prev_page = 1;
constexpr CacheEntry current_page = 2;
constexpr CacheEntry next_page = 3;
constexpr CacheEntry next_next_page = 4;
constexpr CacheEntry next_next_next_page = 5;

PageView::PageView(QQuickItem *parent) : QQuickPaintedItem(parent), m_pageOffset(0), m_pageNumber(1), m_pageCount(1)
{
    this->setOpaquePainting(true); // we fill everything, hence use opaque painting for improved speed
    this->setMipmap(true);

    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    auto metaEnum = QMetaEnum::fromType<Reading::ColorScheme>();
    auto defaultVal = metaEnum.valueToKey(static_cast<int>(Reading::ColorScheme::DARK));
    const auto loaded  = settings.value(QStringLiteral("colorScheme"), defaultVal).toString().toStdString();
    setColorScheme(static_cast<Reading::ColorScheme>(metaEnum.keyToValue(loaded.c_str())));

    m_font = settings.value(QStringLiteral("font"), QFont(QStringLiteral("Literata"), 14)).value<QFont>();
    QMutexLocker lock {&m_documentMutex};
    m_textDocument.setDefaultFont(m_font);
    emit fontChanged();

    m_textDocument.setDefaultStyleSheet(QStringLiteral("p {text-indent: 10px; margin-top: 0; margin-bottom: 0;} .image {text-indent: 0px; margin-bottom: 8px}"));
    m_offsetAnimation = new QPropertyAnimation(this, "pageOffset", this);
    m_offsetAnimation->setDuration(200);
    m_offsetAnimation->setEasingCurve(QEasingCurve::InOutSine);

    connect(this, &PageView::widthChanged, this, &PageView::handleResize, Qt::QueuedConnection);
    connect(this, &PageView::heightChanged, this, &PageView::handleResize, Qt::QueuedConnection);
}

namespace  {
const constexpr auto betweenPagesOffset = 30;

[[nodiscard]] inline QImage resizeIfNecessary(const QImage image, qreal width, qreal height, qreal margin) {
    // currently affected by QTBUG 73730; no workaround is attempted
    auto pxr = qGuiApp->devicePixelRatio();
    margin *= 3; // top + bottom, even though it shouldn't be necessary?
    width  = (width - margin) * pxr;
    height = (height - margin) * pxr;
    if (image.height() >= height || image.width() >= width) {
        auto scaled = image.scaled( QSize { static_cast<int>(std::floor(width)), static_cast<int>(std::floor(height))}, Qt::KeepAspectRatio, Qt::SmoothTransformation);
        return scaled;
    } else {
        return image;
    }
}
}

void PageView::handleResize() {
    const auto pageWidth =  this->pageWidth();
    const auto pageHeight = this->height();

    if (pageWidth <= 0 || pageHeight <= 0) {
        // happens on Android, causes crashes if we do not exit here
        return;
    }

    m_documentMutex.lock();
    auto previousSize = m_textDocument.pageSize();
    m_documentMutex.unlock();
    if (qFuzzyCompare(pageWidth, previousSize.width()) && qFuzzyCompare(pageHeight, previousSize.height())) {
        return;
    }

    m_documentMutex.lock();
    m_textDocument.setPageSize(QSizeF {pageWidth, pageHeight});
    m_documentMutex.unlock();
    emit parsingStarted();
    if (!m_URL2originalImage.empty()) {
        m_documentMutex.lock();
        const auto html = m_textDocument.toHtml();
        const auto margin = m_textDocument.documentMargin();
        m_textDocument.clear();
        m_documentMutex.unlock();

        if (m_URL2originalImage.size() == 1) {
            // optimize for common case by not creating a function object and starting the QtConcurrent machinery
            auto [path, image] = *m_URL2originalImage.constKeyValueBegin();
            QMutexLocker lock(&m_documentMutex);
            m_textDocument.addResource(QTextDocument::ImageResource, path, resizeIfNecessary(image, pageWidth, pageHeight, margin));
        } else {
            using ImageNamePair = std::pair<QString, QImage>;
            std::function<ImageNamePair (ImageNamePair)> func = [pageWidth, pageHeight, margin]( std::pair<QString, QImage> p ) {
                return std::make_pair<>(p.first, resizeIfNecessary(std::move(p.second), pageWidth, pageHeight, margin));
            };

            const auto resultList = QtConcurrent::blockingMapped<boost::container::small_vector< ImageNamePair,2>>(m_URL2originalImage.constKeyValueBegin(), m_URL2originalImage.constKeyValueEnd(), func);
            for (const auto& result:  resultList) {
                QMutexLocker lock(&m_documentMutex);
                m_textDocument.addResource(QTextDocument::ImageResource, result.first, result.second);
            }
        }

        m_documentMutex.lock();
        m_textDocument.setHtml(html);
        m_documentMutex.unlock();
    }
    emit parsingEnded();

    m_documentMutex.lock();
    const auto newPageCount = m_textDocument.pageCount();
    m_documentMutex.unlock();
    if (m_pageCount != newPageCount) {
        // try to find a close position on page
        const int newPageNumber = std::max(static_cast<int>(std::floor(m_pageNumber * m_pageCount / newPageCount)), 1);
        m_pageCount = newPageCount;
        emit pageCountChanged();
        setPageNumber(newPageNumber);
    }
    asyncPopulateCache();
}

void PageView::setupPainter(QPainter* painter) {
    painter->setRenderHint(QPainter::TextAntialiasing);
    auto bgBrush = QBrush{painter->background()};
    using namespace Reading;
    bgBrush.setColor(mode2bgColor(m_currentColorScheme));
    painter->setBackground(bgBrush);
    auto pen = QPen{painter->pen()};
    pen.setColor(mode2fgColor(m_currentColorScheme));
    painter->setPen(pen);
    painter->setFont(m_font);
}

void PageView::renderPage(QPainter* painter, int pageNumber) {
    const auto pageWidth = this->pageWidth();
    const auto pageHeight = this->height();
    const QRectF textRect = QRectF {0, 0, pageWidth, pageHeight};
    qreal documentHeight, documentWidth;
    {
        QMutexLocker lock(&m_documentMutex);
        documentHeight = m_textDocument.pageSize().height();
        documentWidth = m_textDocument.pageSize().width();
    }
    const QRectF textPageRect(0, pageNumber * documentHeight, documentWidth, documentHeight);
    setupPainter(painter);
    painter->save();
    // Clip the drawing so that the text of the other pages doesn't appear in the margins
    painter->setClipRect(textRect);
    // Translate so that 0,0 is now the page corner
    painter->translate(0, -textPageRect.top());
    // Translate so that 0,0 is the text rect corner
    painter->translate(textRect.left(), textRect.top());

    // using the context is necessary, else the font color is wrong
    QAbstractTextDocumentLayout::PaintContext ctx;
    ctx.palette.setColor(QPalette::Text, painter->pen().color());
    ctx.clip = textPageRect;

    QMutexLocker lock(&m_documentMutex);
    m_textDocument.documentLayout()->draw(painter, ctx);
    painter->restore();
}

void PageView::addCacheEntry(const CacheEntry entry, const int page) {
    const auto idx = static_cast<PageCache::size_type>(entry);
    const auto pixelRatio = qGuiApp->devicePixelRatio();
    const auto adjustedWidth = static_cast<int>(this->pageWidth()*pixelRatio);
    const auto adjustedHeight = static_cast<int>(this->height()*pixelRatio);
    m_pageCache[idx] = {}; // clear memory of current image before creating a new one
    if (doesPageContainImportantPicture(page)) {
        m_pageCache[idx] = QImage {adjustedWidth, adjustedHeight, QImage::Format_ARGB32_Premultiplied};
    } else {
        m_pageCache[idx] = QImage {adjustedWidth, adjustedHeight, QImage::Format_RGB16};
    }
    m_pageCache[idx].setDevicePixelRatio(pixelRatio);
    m_pageCache[idx].fill(Reading::mode2bgColor(m_currentColorScheme));
    QPainter painter(&m_pageCache[idx]);
    renderPage(&painter, page);
}

void PageView::populateCache() {
    const auto pageWidth = static_cast<int>(this->pageWidth());
    const auto pageHeight = static_cast<int>(this->height());
    if (pageWidth <= 0 || pageHeight <= 0) {
        return;
    }

    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        addCacheEntry(prev_prev_page, currentPage-2);
    }
    addCacheEntry(prev_page, currentPage-1);
    addCacheEntry(current_page, currentPage);
    addCacheEntry(next_page, currentPage+1);
    if (m_useTwoPages) {
        addCacheEntry(next_next_page, currentPage+2);
        addCacheEntry(next_next_next_page, currentPage+3);
    }
}

void PageView::asyncPopuateHelper() {
    this->populateCache();
    this->update();
}

void PageView::asyncPopulateCache() {
    {
        QMutexLocker lock(&m_documentMutex);
        if (m_textDocument.toRawText().isEmpty())
                return;
    }
    QTimer::singleShot(0, this, &PageView::asyncPopuateHelper);
}

void PageView::cacheBackward(int dist) {
    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        std::rotate(m_pageCache.rbegin(), m_pageCache.rbegin()+dist, m_pageCache.rend());
        addCacheEntry(prev_prev_page, currentPage-2);
        if (dist == 2)
            addCacheEntry(prev_page, currentPage-1);
    } else {
        m_pageCache[next_page] = std::move(m_pageCache[current_page]);
        m_pageCache[current_page] = std::move(m_pageCache[prev_page]);
        addCacheEntry(prev_page, currentPage-1);
    }
    this->update();
}

void PageView::cacheForward(int dist) {
    int currentPage = m_pageNumber - 1;
    if (m_useTwoPages) {
        std::rotate(m_pageCache.begin(), m_pageCache.begin()+dist, m_pageCache.end());
        addCacheEntry(next_next_next_page, currentPage+3);
        if (dist == 2)
            addCacheEntry(next_next_page, currentPage+2);
    } else {
        // rotate deemed too inpractical for the small range
        // it is VITAL to use std::copy, as the copy constructor seems to be still broken @re:https://bugreports.qt.io/browse/QTBUG-58653
        m_pageCache[prev_page] = std::move(m_pageCache[current_page]);
        m_pageCache[current_page] = std::move(m_pageCache[next_page]);
        addCacheEntry(next_page, currentPage+1);
    }
    this->update();
}


void PageView::drawPageSeparator(QPainter* painter, double initialX) {
    using namespace Reading;
    auto pageWidth = this->pageWidth();
    auto pageHeight = this->height();
    // initialX = pageOffset() + pageWidth
    painter->save();{
        {
            QRectF rectangle1 {initialX, 0, betweenPagesOffset/2, pageHeight};
            QLinearGradient gradient {initialX, 0, pageWidth + betweenPagesOffset/2, 0};
            gradient.setColorAt(0, mode2bgColor(m_currentColorScheme));
            gradient.setColorAt(1, mode2bgColor(m_currentColorScheme).darker());
            QBrush brush{gradient};
            painter->fillRect(rectangle1, brush);
        }
        {
            QRectF rectangle2 {initialX + betweenPagesOffset/2, 0, betweenPagesOffset/2, pageHeight};
            QLinearGradient gradient {initialX + betweenPagesOffset/2, 0, pageWidth + betweenPagesOffset, 0};
            gradient.setColorAt(0, mode2bgColor(m_currentColorScheme).darker());
            gradient.setColorAt(1, mode2bgColor(m_currentColorScheme));
            QBrush brush {gradient};
            painter->fillRect(rectangle2, brush);
        }
    }painter->restore();
}

void PageView::paint(QPainter *painter)
{
    const auto pageWidth =  this->pageWidth();
    const auto pageHeight = this->height();
    const auto pixelRatio = qGuiApp->devicePixelRatio();

    const QRectF target = QRectF {0.0 + pageOffset(), 0.0, pageWidth, pageHeight};
    const QRectF source = QRectF {0.0, 0.0, pageWidth*pixelRatio, pageHeight*pixelRatio};

    painter->drawImage(target, m_pageCache[current_page], source);
    auto totalXOffset = 0.0;

    if (m_useTwoPages) {
        totalXOffset = pageWidth + betweenPagesOffset;
        // draw second page
        const QRectF target2 = QRectF {pageOffset() + totalXOffset, 0, pageWidth, pageHeight};
        painter->drawImage(target2, m_pageCache[next_page], source);
        // draw separator line
        drawPageSeparator(painter, pageOffset() + pageWidth);
    }

    if (pageOffset() == 0.0) {
        return; // no need to draw anything further if no animation is running
    }

    if (pageOffset() <= 0) {
        totalXOffset += pageWidth + pageOffset();
        drawPageSeparator(painter, totalXOffset);
        totalXOffset += betweenPagesOffset;
        const QRectF target1 = QRectF {totalXOffset, 0.0, pageWidth, pageHeight};
        painter->drawImage(target1, m_pageCache[m_useTwoPages ? next_next_page : next_page], source);
        if (m_useTwoPages) {
            totalXOffset += pageWidth;
            drawPageSeparator(painter, totalXOffset);
            totalXOffset += betweenPagesOffset;
            const QRectF target2 = QRectF {totalXOffset, 0.0, pageWidth, pageHeight};
            painter->drawImage(target2, m_pageCache[next_next_next_page], source);
        }
    } else {
        totalXOffset = -betweenPagesOffset + pageOffset() ;
        drawPageSeparator(painter, totalXOffset);
        totalXOffset -= pageWidth;
        const QRectF target1 = QRectF {totalXOffset, 0.0, pageWidth, pageHeight};
        painter->drawImage(target1, m_pageCache[prev_page], source);
        if (m_useTwoPages) {
            totalXOffset -= betweenPagesOffset;
            drawPageSeparator(painter, totalXOffset);
            totalXOffset -= pageWidth;
            const QRectF target2 = QRectF {totalXOffset, 0.0, pageWidth, pageHeight};
            painter->drawImage(target2, m_pageCache[prev_prev_page], source);
        }
    }
}

bool PageView::doesPageContainImportantPicture(int pageNumber)
{
    if (pageNumber < 0 or pageNumber > pageCount()) {
        // can happen when we render off-screen pages at the end and beginning of the text
        return false;
    }
    // assumption pageNumber is already 0 based
    QMutexLocker lock(&m_documentMutex);
    auto layout = m_textDocument.documentLayout();
    // position cursor in center of page
    // TODO: there is probably a better point than the center (and is one single point enough?)
    auto x = this->pageWidth() / 2;
    auto y = (pageNumber + 0.5) * this->height();
    auto position = layout->hitTest({x, y}, Qt::FuzzyHit);
    if (position == -1)
        position = 0;
    QTextCursor cursor {&m_textDocument};
    cursor.setPosition(position);
    auto format = cursor.charFormat();
    return format.isImageFormat();
}

QPointF PageView::translatePageViewCoordinates2DocumentCoordinates(qreal x, qreal y)
{
    auto width = this->pageWidth();
    auto height = this->height();
    // assumption: x and y are always inside the PageView area
    if (x > width) {
        // we are on the second page, translate the x and y coordinate
        x -= width + betweenPagesOffset;
        y += (m_pageNumber - 1 /*offset for pageNumber is 1*/ + 1 /*we are on the second page*/) * height;
    } else {
        // we are on the first page, so we only need to change the y value
        y += (m_pageNumber - 1 /*offset for pageNumber is 1*/) * height;
    }
    return {x, y};
}

void PageView::handleClick(double x, double y)
{
    Q_UNUSED(x)
    Q_UNUSED(y)
}

void PageView::setText(const QString& part, qreal percentage) {

    emit parsingStarted();
    auto asyncSetText = [this, part, percentage](){
        {
            QMutexLocker lock(&m_documentMutex);
            m_URL2originalImage.clear();
        }
        auto manager =  QQmlEngine::contextForObject(this)->engine()->networkAccessManager();
        QString wellFormedHtml = QStringLiteral("<!DOCTYPE html><html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"></head><body>%1</body></html>").arg(part);

        // extract all image URLs (the HTML should be simple enough in this case to make the RegEx approach feasible
        QRegularExpression re(QStringLiteral(R"|(<img\s*src="([^"]*)"\s*(/?)>)|"));
        QRegularExpressionMatchIterator it = re.globalMatch(wellFormedHtml);
        boost::container::small_vector<QString, 2> imageURLS {}; // most parts do not have many images, so we use a small_vector with a small capacity
        while (it.hasNext()) {
            imageURLS.push_back(it.next().captured(1));
        }
        // wrap img tags in p tags (cannot use capture group 0 in replace)
        // and replace centerp with align=center
        QRegularExpression re_centerp(QStringLiteral(R"|(<p class="centerp">)|"));
        QRegularExpression re_normalp(QLatin1String("<p>"));
        auto fixedUpDocument = wellFormedHtml.replace(re, QLatin1String(R"|(<p align="center" class="image"><img src="\1"></p>)|"))
                                             .replace(re_centerp, QLatin1String(R"|(<p align="center">)|"))
                                             .replace(re_normalp, QLatin1String(R"|(<p align="justify">)|"));

        using namespace AsyncFuture;
        Combinator workDone = combine(AllSettled);
        for (const auto& imageURL: qAsConst(imageURLS)) {
            QNetworkRequest req(QUrl {imageURL});
            QNetworkReply* reply = manager->get(req);
            workDone << observe(reply, &QNetworkReply::finished).subscribe([this, reply, imageURL](){
                reply->deleteLater();
                if (reply->error()) {
                    // ensure that broken images don't crash the reader
                    qDebug() << "error fetching an image:" << reply->errorString();
                    return QFuture<void>();
                }
                QImage image;
                image.loadFromData(reply->readAll());
                image.setDevicePixelRatio(qGuiApp->devicePixelRatio());
                qreal margin {};
                {
                    QMutexLocker lock(&m_documentMutex);
                    margin = m_textDocument.documentMargin();
                    m_URL2originalImage[imageURL] = image;
                }
                const auto pageWidth = this->pageWidth();
                const auto pageHeight = this->height();
                return QtConcurrent::run([this, image, pageWidth, pageHeight, imageURL, margin](){
                    auto imageVariant = QVariant{resizeIfNecessary(image, pageWidth, pageHeight, margin)};
                    QMutexLocker lock(&m_documentMutex);
                    m_textDocument.addResource(QTextDocument::ImageResource, QUrl(imageURL), imageVariant);
                });
            }).future();
        }

        workDone.onFinished([this, fixedUpDocument, percentage]() {
            {
                QMutexLocker lock(&m_documentMutex);
                m_textDocument.setHtml(fixedUpDocument);
                m_pageCount = m_textDocument.pageCount();
                m_pageNumber = std::max<int>(1, static_cast<int>(m_pageCount*percentage));
                emit pageCountChanged();
                emit pageNumberChanged();
            }
            this->populateCache();
            this->update();

            emit parsingEnded();
        });
    };
    QTimer::singleShot(0, asyncSetText);
}

void PageView::setFont(const QFont &newFont)
{
    if (m_font == newFont) {
        return;
    }

    m_font = newFont;
    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    settings.setValue(QStringLiteral("font"), newFont);
    emit fontChanged();
    {
        QMutexLocker lock(&m_documentMutex);
        m_textDocument.setDefaultFont(newFont);
        // inspired from https://stackoverflow.com/questions/27716625/qtextedit-change-font-of-individual-paragraph-block/27719789#27719789
        for (auto textBlock = m_textDocument.begin(); textBlock != m_textDocument.end(); textBlock = textBlock.next()) {
            if (!textBlock.isValid()) {
                qDebug() << "invalid textblock";
                continue;
            }
            QTextCursor editingCursor(textBlock);
            if (editingCursor.isNull()) {
                qDebug() << "isNull";
                continue;
            }
            for (auto it = textBlock.begin();
                 !it.atEnd();
                 ++it) {
                if (!it.fragment().isValid()) {
                    qDebug() << "invalid fragment";
                    continue;
                }
                // When changing the font,one has to be careful: QTextDocument wants <em> tags to be in italics. Assigning a font which is normal or bold would lead to crashes
                auto charFormat = it.fragment().charFormat();
                QFont oldFont = charFormat.font();
                oldFont.setFamily(m_font.family());
                oldFont.setPointSizeF(m_font.pointSizeF());
                charFormat.setFont(oldFont);
                editingCursor.setPosition(it.fragment().position());
                editingCursor.setPosition(it.fragment().position() + it.fragment().length(), QTextCursor::KeepAnchor);
                editingCursor.setCharFormat(charFormat);
            }
        }
    }
    asyncPopulateCache();
}

QFont PageView::font()
{
    return m_font;
}

void PageView::resetPageOffset()
{
    m_offsetAnimation->setEndValue(0);
    m_offsetAnimation->start();
}

qreal PageView::pageOffset() const
{
    return m_pageOffset;
}

void PageView::setColorScheme(Reading::ColorScheme newColorScheme)
{
    using namespace Reading;
    setFillColor(mode2bgColor(newColorScheme));
    if (m_currentColorScheme != newColorScheme) {
        m_currentColorScheme = newColorScheme;
        QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
        settings.setValue(QStringLiteral("colorScheme"), QMetaEnum::fromType<Reading::ColorScheme>().valueToKey(static_cast<int>(m_currentColorScheme)));
        emit colorSchemeChanged();
        asyncPopulateCache();
    }
}

void PageView::setPageOffset(qreal pageOffset)
{
    if (m_useTwoPages)
        pageOffset = qBound(2*(-pageWidth() - betweenPagesOffset) , pageOffset, 2*(pageWidth() + betweenPagesOffset));
    else
        pageOffset = qBound(-pageWidth() - betweenPagesOffset , pageOffset, pageWidth() + betweenPagesOffset);
    if (m_pageOffset != pageOffset) {
        m_pageOffset = pageOffset;
        this->update(); // necessary for the animation to work
    }
}

int PageView::colorScheme() {
    return static_cast<int>(m_currentColorScheme);
}

qreal PageView::pageWidth() const {
    return m_useTwoPages ? ((this->width() - betweenPagesOffset) / 2) : this->width();
}

bool PageView::useTwoPages() const
{
    return m_useTwoPages;
}

void PageView::setuseTwoPages(bool useTwoPages)
{
    if (useTwoPages == m_useTwoPages) {
        return;
    }
    m_useTwoPages = useTwoPages;
    if (useTwoPages) {
        m_offsetAnimation->setDuration(550);
    } else {
        m_offsetAnimation->setDuration(200);
    }
    QSettings settings(QStringLiteral("J-novel.club"), QStringLiteral("reader"));
    settings.setValue(QStringLiteral("useTwoPages"), useTwoPages);
    emit useTwoPagesChanged();
    this->handleResize();

}

int PageView::pageCount() const
{
    return m_pageCount;
}

int PageView::pageNumber() const
{
    return m_pageNumber;
}

void PageView::keepScreenOn(bool enabled) {
#ifdef Q_OS_ANDROID
  QtAndroid::runOnAndroidThread([enabled]{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid()) {
      QAndroidJniObject window =
          activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

      if (window.isValid()) {
        const int FLAG_KEEP_SCREEN_ON = 128;
        if (enabled) {
          window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        } else {
          window.callMethod<void>("clearFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        }
      }
    }
    QAndroidJniEnvironment env;
    if (env->ExceptionCheck()) {
      env->ExceptionClear();
    }
  });
#else
    // unnecessary on desktop operating systems
    // unsupported on iOS
    Q_UNUSED(enabled);
#endif
}

void PageView::setPageNumber(int pageNumber)
{
    pageNumber = qBound(1, pageNumber, m_pageCount);
    if (m_pageNumber == pageNumber) {
        return;
    }
    if (m_offsetAnimation->state() == QPropertyAnimation::Running) {
        m_offsetAnimation->stop();
        // FIXME: sipmly returnung is not the best idea as we theoretically loose the call
        // however, this can basically only happen if the arrow button is constantly pressed
        // or if the page move button is constantly clicked. In that case we get the update anyway
        return;
    }
    auto oldNumber = m_pageNumber;
    m_pageNumber = pageNumber;
    emit pageNumberChanged();
    emit percentReadChanged(m_pageNumber/ static_cast<qreal>(m_pageCount));
    const auto pageNumDifference = std::abs(oldNumber - pageNumber);
    if (pageNumDifference > 2) {
        // larger jump, do not use offset animation
        m_pageOffset = 0;
        this->asyncPopulateCache();
        return;
    }
    double animationEndValue = 0;
    auto const goPrev = pageNumber < oldNumber;
    if (goPrev) {
        animationEndValue =  pageWidth() + betweenPagesOffset;
        if (m_useTwoPages) {
            animationEndValue += betweenPagesOffset;
        }
    } else {
        animationEndValue = -pageWidth() - 2*betweenPagesOffset;
        if (m_useTwoPages) {
            animationEndValue -= betweenPagesOffset;
        }
    }
    animationEndValue *= pageNumDifference;
    m_offsetAnimation->setEndValue(animationEndValue);
    // setup additional page
    auto const connection = new QMetaObject::Connection;
    *connection = connect(m_offsetAnimation, &QPropertyAnimation::stateChanged, this, [this, connection, pageNumDifference, goPrev](QAbstractAnimation::State newState, QAbstractAnimation::State oldState) {
        Q_UNUSED(oldState);
        if (newState != QAbstractAnimation::Stopped) {
            return;
        }
        disconnect(*connection);
        delete connection;
        m_pageOffset = 0; // reset page_Offset
        // using async updates is a) not required, because the signal is already hanlded async, and b) would cause flickering
        if (goPrev) {
            this->cacheBackward(pageNumDifference);
        } else {
            this->cacheForward(pageNumDifference);
        }
    });
    m_offsetAnimation->start();
}
