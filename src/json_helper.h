#ifndef JSON_HELPER_H
#define JSON_HELPER_H

#include <QString>

class QJsonArray;
class QJsonObject;

struct ParsedAttachment {
    QString coverPath;
    QString thumbnailPath;
};

Q_DECLARE_TYPEINFO(ParsedAttachment, Q_MOVABLE_TYPE);

ParsedAttachment parseAttachments(const QJsonArray& attachments);

QString partJson2thumbnailPath(const QJsonObject& part);

#endif
