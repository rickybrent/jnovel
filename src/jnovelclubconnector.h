#ifndef JNOVELCLUBCONNECTOR_H
#define JNOVELCLUBCONNECTOR_H

#include <QObject>
#include <QString>

#include "serieslist.h"
#include "volumelist.h"
#include "user.h"
#include "parts.h"
#include "partlist.h"
#include "readingmode.h"

class QNetworkAccessManager;
class QSettings;
class QNetworkReply;

template<class T>
using observer_ptr = T*;

class JNovelClubConnector : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY(JNovelClubConnector)

    Q_PROPERTY(bool loggedIn READ loggedIn NOTIFY loggedInChanged)
    Q_PROPERTY(Part lastFetchedPart MEMBER m_lastFetchedPart NOTIFY lastFetchedPartChanged)
    Q_PROPERTY(int loginStatus READ loginStatus NOTIFY loginStatusChanged)

    enum LoginStatus {
        LoggedOff = 0,
        Pending = 1,
        Failed = 2,
        LoggedIn = 3
    };

public:
    explicit JNovelClubConnector(QNetworkAccessManager *manager, QObject *parent = nullptr);

    void setAccessToken(const QString &accessToken);

    bool loggedIn();

    int loginStatus() const;

signals:
    void LNSeriesFetched(const SeriesList& series);
    void MangaSeriesFetched(const SeriesList& series);
    void LNVolumesFetched(const VolumeList& volumes);
    void MangaVolumesFetched(const VolumeList& volumes);
    void partFetched(const QString& html, qreal percentageRead);
    void loginSuccess();
    void loginFailed(const QString& errorMessage);
    void loggedInChanged();
    void fetchedUserData(const User& user);
    void fetchedLatestReleasedParts(const PartList& parts);
    void resetLatestReleasedParts(const PartList& parts);
    void lastFetchedPartChanged();

    void loginStatusChanged(int loginStatus);

public slots:
    void fetchLNSeriesList();
    void fetchMangaSeriesList();
    void fetchLNVolumesForID(const QString& seriesID);
    void fetchMangaVolumesForID(const QString& seriesID);
    void refreshVolumes();
    void fetchPart(const Part part, qreal percentageRead = 0.0);
    bool checkIfLoggedIn();
    void fetchUserData();
    void login(const QString& email, const QString& pass);
    void loginWithStoredCredentials();
    void updatePartCompletionStatus(const Part& part, qreal readPercentage);
    void fetchLatestReleasedParts(int offset=-1, unsigned limit=10);
    QString createAccessCookiesString();

private:
    observer_ptr<QNetworkAccessManager> m_manager;
    QSettings* m_settings;
    QString m_currentSeriesID;
    QString m_accessToken;
    QString m_userid;
    Part m_lastFetchedPart;
    int m_loginStatus = Pending;
    int m_alreadyFetchedCount = 0;
    void handleNetworkError(QNetworkReply* reply);
};


#endif // JNOVELCLUBCONNECTOR_H
