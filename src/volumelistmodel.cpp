#include "volumelistmodel.h"

#include <algorithm>
#include <iterator>

VolumeListModel::VolumeListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QVariant VolumeListModel::headerData(int , Qt::Orientation , int ) const
{
    return QVariant();
}

int VolumeListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return static_cast<int>(m_volumes.size());
}

QHash<int, QByteArray> VolumeListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[TitleRole] = "title";
    roles[TitleSlugRole] = "titleSlug";
    roles[AuthorRole] = "author";
    roles[IllustratorOrArtistRole] = "illustratorOrArtist";
    roles[TranslatorRole] = "translator";
    roles[EditorRole] = "editor";
    roles[DescriptionRole] = "description";
    roles[TagsRole] = "tags";
    roles[DescriptionShortRole] = "shortDescription";
    roles[ImageURLRole] = "imageURL";
    roles[PreviewImageURLRole] = "previewImageURL";
    roles[IDRole] = "volumeID";
    return roles;
}

QVariant VolumeListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() < 0 || static_cast<decltype (m_volumes.size())>(index.row()) >= m_volumes.size())
        return QVariant();

    auto& volume = m_volumes.at(static_cast<decltype(m_volumes.size())>(index.row()));
    switch (role) {
    case Qt::DisplayRole:
        //fallthrough
    case TitleRole:
        return volume.title();
    case AuthorRole:
        return volume.author();
    case IllustratorOrArtistRole:
        return volume.illustratorOrArtist();
    case TranslatorRole:
        return volume.translator();
    case EditorRole:
        return volume.editor();
    case DescriptionRole:
        return volume.description();
    case TagsRole:
        return volume.tags();
    case ImageURLRole:
        return volume.imageUrl();
    case PreviewImageURLRole:
        return volume.previewImageUrl();
    case IDRole:
        return volume.volumeID();
    case TitleSlugRole: {
        return volume.titleslug();
    }
    default:
        return QVariant();
    }
}

void VolumeListModel::selectPartList(int indexNumber)
{
    emit partListChanged(m_volumes.at(indexNumber).parts());
}

void VolumeListModel::clear()
{
    beginResetModel();
    m_volumes.clear();
    emit partListChanged(PartList{});
    endResetModel();
}

PartList VolumeListModel::getPartsForVolume(const QString& volumeID)
{
    auto it = std::find_if(std::cbegin(m_volumes), std::cend(m_volumes), [&volumeID](const auto& volume) {
        return volume.volumeID() == volumeID;
    });
    if (it == std::cend(m_volumes)) {
        return {};
    }
    return it->parts();
}


void VolumeListModel::changeVolume ( const VolumeList& newVolumes )
{
    beginResetModel();
    m_volumes = newVolumes;
    endResetModel();
}

namespace {
    inline auto findPart(const PartList& partlist, const QString& partID) {
        return std::find_if(std::cbegin(partlist), std::cend(partlist), [&partID](const auto& curPart){
            return partID == curPart.partID();
        });
    }

    inline auto findVolume(const VolumeList& volList, const QString& volumeID) {
        return std::find_if(std::cbegin(volList), std::cend(volList), [&volumeID](const auto& curVolume){
            return volumeID == curVolume.volumeID();
        });
    }

    bool is_accessible(const Part& part) {return !part.expired();}
}

Part VolumeListModel::getNextPart(const Part& part)
{
    Part ret;
    auto volumeIt = findVolume(m_volumes, part.volumeID());
    if (volumeIt == std::cend(m_volumes)) {
        return ret;
    }
    const auto& parts = volumeIt->parts();
    auto partIt = findPart(parts, part.partID());
    Q_ASSERT(partIt != std::cend(parts)); // the part should definitely be there
    ++partIt;
    if (partIt != std::cend(parts)) {
        ret = *partIt;
    } else {
        ++volumeIt;
        if (volumeIt != std::cend(m_volumes)) {
            ret = Part {volumeIt->parts().at(0) };
        }
    }
    if (ret.partID() != "" && is_accessible(ret))
        return ret;
    return Part {};
}

Part VolumeListModel::getPrevPart(const Part& part)
{
    auto volumeIt = findVolume(m_volumes, part.volumeID());
    if(volumeIt == std::cend(m_volumes)) {
        return Part {};
    }
    auto ret = Part {};
    const auto& parts = volumeIt->parts();
    auto partIt = findPart(parts, part.partID());
    Q_ASSERT(partIt != std::end(parts)); // the part should definitely be there
    if (partIt != std::cbegin(parts)) {
        ret = *(--partIt);
    } else {
        ret = (volumeIt == std::cbegin(m_volumes)) ? Part {} : (--volumeIt)->parts().back();
    }
    if (is_accessible(ret))
        return ret;
    return Part {};
}


void LibraryModel::setUser ( const User& user )
{
    m_user = user;
    m_volumes = user.getLibrary();
}

qreal LibraryModel::getPercentRead(const QString &partid)
{
    const auto part2completionStatus = m_user.getPart2completionStatus();
    if (part2completionStatus.contains(partid))
        return part2completionStatus[partid].completion;
    else
        return 0;
}


void LibraryModel::updatePercentRead(qreal percentRead, const Part &part)
{
    m_user.setPartCompletion(part.partID(), percentRead);
}


