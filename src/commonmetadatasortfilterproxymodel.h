#ifndef COMMONMETADATASORTFILTERPROXYMODEL_H
#define COMMONMETADATASORTFILTERPROXYMODEL_H

#include <qobjectdefs.h>
#include <QSortFilterProxyModel>
#include <QRegularExpression>

class QString;

class CommonMetaDataSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_DISABLE_COPY(CommonMetaDataSortFilterProxyModel)

public:
    CommonMetaDataSortFilterProxyModel(QObject* parent = nullptr);

public slots:
    void setFilterRegularExpression(const QString& regex);


protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    //bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    QRegularExpression m_regexp = {};
};

#endif // COMMONMETADATASORTFILTERPROXYMODEL_H
