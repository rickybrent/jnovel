#ifndef VOLUMELISTMODEL_H
#define VOLUMELISTMODEL_H

#include <QAbstractListModel>
#include "volume.h"
#include "user.h"
#include "parts.h"
#include "partlist.h"

class QString;

class VolumeListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DISABLE_COPY(VolumeListModel)

public:

    explicit VolumeListModel(QObject *parent = nullptr);

    enum VolumeRoles {
        TitleRole = Qt::UserRole + 1,
        AuthorRole,
        IllustratorOrArtistRole,
        TranslatorRole,
        EditorRole,
        DescriptionRole,
        TagsRole,
        DescriptionShortRole,
        ImageURLRole,
        PreviewImageURLRole,
        TitleSlugRole,
        IDRole
    };

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
    void changeVolume(const VolumeList& newVolumes);
    PartList getPartsForVolume(const QString& volumeID);
    Part getNextPart(const Part& part);
    Part getPrevPart(const Part& part);
    void selectPartList(int indexNumber);
    void clear();

signals:
    void partListChanged(const PartList& newParts);

protected:
    QHash<int, QByteArray> roleNames() const override;
    VolumeList m_volumes = {};
};

class LibraryModel : public VolumeListModel {
    Q_OBJECT
    Q_DISABLE_COPY(LibraryModel)

public:
    LibraryModel(QObject* parent) : VolumeListModel(parent) {}

public slots:
    void setUser(const User& user);
    qreal getPercentRead(const QString& partid);
    void updatePercentRead(qreal percentRead, const Part& part);

private:
    User m_user = {};
};

#endif // VOLUMELISTMODEL_H
