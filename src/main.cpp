#include <QQuickStyle>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQml/qqmlcontext.h>
#include <QQmlEngine>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QIcon>
#include <QQmlNetworkAccessManagerFactory>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QtWebView>

#include "pageview.h"
#include "commonmetadatasortfilterproxymodel.h"
#include "jnovelclubconnector.h"
#include "serieslistmodel.h"
#include "series.h"
#include "volumelistmodel.h"
#include "partlistmodel.h"

class OnDiskCacheNAMFactory : public QQmlNetworkAccessManagerFactory
{
public:
    QNetworkAccessManager *create(QObject *parent) final;
};

QNetworkAccessManager *OnDiskCacheNAMFactory::create(QObject *parent)
{
    auto *nam = new QNetworkAccessManager(parent);
    auto diskCache = new QNetworkDiskCache();
    auto cachePath = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    diskCache->setCacheDirectory(cachePath);
    nam->setCache(diskCache);
    return nam;
}

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QtWebView::initialize();
    QGuiApplication::setOrganizationName("J-novel.club");
    QGuiApplication::setApplicationName("reader");
#ifdef Q_OS_WIN
    QQuickStyle::setStyle(QStringLiteral("Universal"));
#elif defined Q_OS_ANDROID
    QQuickStyle::setStyle(QStringLiteral("Material"));
#else
    QQuickStyle::setStyle(QStringLiteral("Fusion"));
#endif
    QQmlApplicationEngine engine {};
    engine.setNetworkAccessManagerFactory(new OnDiskCacheNAMFactory);

    JNovelClubConnector connector{engine.networkAccessManager()};

    auto LNSeriesModel = new SeriesListModel(&app);
    auto MangaSeriesModel = new SeriesListModel(&app);
    auto filterableLNSeriesModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableLNSeriesModel->setSourceModel(LNSeriesModel);
    auto filterableMangaSeriesModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableMangaSeriesModel->setSourceModel(MangaSeriesModel);
    auto LNVolumeModel = new VolumeListModel(&app);
    auto filterableLNVolumeModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableLNVolumeModel->setSourceModel(LNVolumeModel);
    auto MangaVolumeModel = new VolumeListModel(&app);
    auto filterableMangaVolumeModel = new CommonMetaDataSortFilterProxyModel(&app);
    filterableMangaVolumeModel->setSourceModel(MangaVolumeModel);
    auto libraryModel = new LibraryModel(&app);
    auto LNPartsModel = new PartListModel(&app);
    auto mangaPartsModel = new PartListModel(&app);
    auto latestReleasedPartsModel = new PartListModel(&app);

    QObject::connect(&connector, &JNovelClubConnector::LNSeriesFetched, LNSeriesModel, &SeriesListModel::setSeriesList);
    QObject::connect(&connector, &JNovelClubConnector::MangaSeriesFetched, MangaSeriesModel, &SeriesListModel::setSeriesList);

    QObject::connect(&connector, &JNovelClubConnector::LNVolumesFetched, LNVolumeModel, &VolumeListModel::changeVolume);
    QObject::connect(&connector, &JNovelClubConnector::MangaVolumesFetched, MangaVolumeModel, &VolumeListModel::changeVolume);

    QObject::connect(&connector, &JNovelClubConnector::loginSuccess, &connector, &JNovelClubConnector::fetchUserData);

    QObject::connect(&connector, &JNovelClubConnector::fetchedUserData, libraryModel, &LibraryModel::setUser);

    QObject::connect(&connector, &JNovelClubConnector::fetchedLatestReleasedParts, latestReleasedPartsModel, &PartListModel::addMoreParts);
    QObject::connect(&connector, &JNovelClubConnector::resetLatestReleasedParts, latestReleasedPartsModel, &PartListModel::setParts);


    QObject::connect(LNVolumeModel, &VolumeListModel::partListChanged, LNPartsModel, &PartListModel::setParts);
    QObject::connect(MangaVolumeModel, &VolumeListModel::partListChanged, mangaPartsModel, &PartListModel::setParts);


    connector.fetchLNSeriesList();

    QQmlContext *context = engine.rootContext();
    qmlRegisterType<PageView>("club.jnovel.unfofficial", 1, 0, "PartPageView");
    qmlRegisterUncreatableMetaObject(Reading::staticMetaObject, "club.jnovel.unfofficial", 1, 0, "ReadingNS", QStringLiteral("only enums!"));

    QFontDatabase::addApplicationFont(QStringLiteral(":/resources/icomoon.ttf"));
    QFontDatabase::addApplicationFont(QStringLiteral(":/resources/literata/literata-regular.ttf"));
    QFontDatabase::addApplicationFont(QStringLiteral(":/resources/literata/literata-bold.ttf"));
    QFontDatabase::addApplicationFont(QStringLiteral(":/resources/literata/literata-italic.ttf"));
    QFontDatabase::addApplicationFont(QStringLiteral(":/resources/literata/literata-bold-italic.ttf"));

    context->setContextProperty(QStringLiteral("filterableLNSeriesModel"), filterableLNSeriesModel);
    context->setContextProperty(QStringLiteral("filterableMangaSeriesModel"), filterableMangaSeriesModel);
    context->setContextProperty(QStringLiteral("LNVolumeModel"), filterableLNVolumeModel);
    context->setContextProperty(QStringLiteral("MangaVolumeModel"), filterableMangaVolumeModel);
    context->setContextProperty(QStringLiteral("LNPartsModel"), LNPartsModel);
    context->setContextProperty(QStringLiteral("MangaPartsModel"), mangaPartsModel);
    context->setContextProperty(QStringLiteral("novelClubConnector"), &connector);
    context->setContextProperty(QStringLiteral("library"), libraryModel);
    context->setContextProperty(QStringLiteral("latestReleasedParts"), latestReleasedPartsModel);

    engine.load(QUrl(QStringLiteral("qrc:qml/main.qml")));

    auto icon = QIcon(QStringLiteral(":/resources/icon.png"));
    QGuiApplication::setWindowIcon(icon);

    return QGuiApplication::exec();
}
