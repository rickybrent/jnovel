#ifndef BOOKTYPE_H
#define BOOKTYPE_H

enum class BookType {
    LightNovel,
    Manga
};

#endif // BOOKTYPE_H
