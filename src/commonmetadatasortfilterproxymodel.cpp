#include "commonmetadatasortfilterproxymodel.h"

#include <QString>
#include <algorithm>

CommonMetaDataSortFilterProxyModel::CommonMetaDataSortFilterProxyModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    m_regexp.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
}

void CommonMetaDataSortFilterProxyModel::setFilterRegularExpression(const QString& regex)
{
    m_regexp.setPattern(regex);
    invalidateFilter();
}

bool CommonMetaDataSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (sourceParent.isValid()) {
        return false; // source model is a list, valid parent doesn't make sense
    }

    if (!m_regexp.isValid()) {
        return true;
    }

    auto index = sourceModel()->index(sourceRow, 0, sourceParent);
    auto allRoles = sourceModel()->roleNames();
    m_regexp.optimize();
    return std::any_of(allRoles.constKeyValueBegin(), allRoles.constKeyValueEnd(), [this, index](const auto& role) {
        const auto data = this->sourceModel()->data(index, role.first).toString();
        return this->m_regexp.match(data).hasMatch();
    });
}
