#ifndef VOLUME_H
#define VOLUME_H

#include "commonmetadata.h"
#include "partlist.h"
#include "parts.h"

class QJsonObject;

class Volume
{
public:
    Volume(const QString& title, const QString& titleslug, const QString& titleShort, const QString& titleOriginal, const QString& author,
           const QString& illustrator, const QString& translator, const QString& editor,
           const QString& description, const QString& descriptionShort, const QString& tags, const QString& imageUrlFragment,
           const QString& previewImageUrlFragment, const QString& id, int volumeNumber, const PartList & parts)
        : m_metaData(title, titleslug, titleShort, titleOriginal, author, illustrator, translator,
                         editor, description, descriptionShort, tags, imageUrlFragment, previewImageUrlFragment),
          m_volumeID(id), m_parts(parts), m_volumeNumber(volumeNumber) {}

    QString volumeID() const;

    static Volume fromJSON(const QJsonObject& volume, const BookType type = BookType::LightNovel);

    PartList parts() const;

#define X(type, name) type name() const;
    CommonMetadataList
#undef X

private:
    CommonMetadata m_metaData;
    QString m_volumeID;
    PartList m_parts;
    int m_volumeNumber;
};

Q_DECLARE_TYPEINFO(Volume, Q_MOVABLE_TYPE);

#endif // VOLUME_H
