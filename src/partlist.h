#ifndef PARTLIST_H
#define PARTLIST_H

#include <boost/container/small_vector.hpp>
#include "parts.h"

// the common case is in the partviewer, where we do not have more than 10 parts most of the time
// exceptions are the Clockwork Planet volumes and the latest parts model
using PartList = boost::container::small_vector<Part, 10>;

#endif // PARTLIST_H
