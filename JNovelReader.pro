# Created by and for Qt Creator This file was created for editing the project sources only.
# You may attempt to use it for building too, by modifying this file here.

TARGET = JNovelReader

QT += core gui network qml quick quickcontrols2 concurrent webview
android {
    QT += androidextras
}

TEMPLATE = app

CONFIG += c++17

HEADERS = \
   src/commonmetadata.h \
   src/commonmetadatasortfilterproxymodel.h \
   src/jnovelclubconnector.h \
   src/json_helper.h \
   src/pageview.h \
   src/partlistmodel.h \
   src/parts.h \
   src/readingmode.h \
   src/series.h \
   src/serieslistmodel.h \
   src/user.h \
   src/volume.h \
   src/volumelistmodel.h \
    src/booktype.h \
    src/partlist.h \
    src/serieslist.h \
    src/volumelist.h

SOURCES = \
   src/commonmetadata.cpp \
   src/commonmetadatasortfilterproxymodel.cpp \
   src/jnovelclubconnector.cpp \
   src/json_helper.cpp \
   src/main.cpp \
   src/pageview.cpp \
   src/partlistmodel.cpp \
   src/parts.cpp \
   src/readingmode.cpp \
   src/series.cpp \
   src/serieslistmodel.cpp \
   src/user.cpp \
   src/volume.cpp \
   src/volumelistmodel.cpp

INCLUDEPATH = \
    src

RESOURCES = jnovelreader.qrc
CONFIG += qtquickcompiler
CONFIG += rtti_off

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x50110

DISTFILES += \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/AndroidManifest.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

