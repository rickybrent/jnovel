cmake_minimum_required(VERSION 3.10)
project(jnovelreader VERSION 0.1.1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
find_package(ECM REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_DIR})
include(ECMEnableSanitizers)

if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Wextra -Wshadow -Wnon-virtual-dtor -Wold-style-cast -Wcast-align -Wunused -Woverloaded-virtual -Wconversion -Wmisleading-indentation -Wduplicated-cond -Wduplicated-branches -Wlogical-op -Wnull-dereference -Wdouble-promotion -Wformat=2")
endif()
set(REQUIRED_QT_VERSION "5.12.0")

if(CMAKE_BUILD_TYPE MATCHES DEBUG)
    add_definitions(-DQT_QML_DEBUG)
endif()

include(CheckIPOSupported)
check_ipo_supported(RESULT supported OUTPUT error)
include(FeatureSummary)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOMOC ON)

find_package(Qt5 ${REQUIRED_QT_VERSION} NO_MODULE COMPONENTS Core Gui Concurrent Network Qml QuickControls2 WebView REQUIRED)
find_package(Qt5QuickCompiler)

qtquick_compiler_add_resources(RESOURCES jnovelreader.qrc)

add_executable(JNovelReader WIN32 src/main.cpp ${RESOURCES})


if( supported )
    message(STATUS "IPO / LTO enabled")
    set_property(TARGET JNovelReader PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
else()
        message(STATUS "IPO / LTO not supported: <${error}>")
endif()


if(ANDROID)
    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android" CACHE INTERNAL "")
endif()


find_package (Threads) # for whatever reason we need to manually add it?
find_package(Boost "1.61.0" REQUIRED)


target_sources(JNovelReader PRIVATE
               src/series.cpp
               src/serieslistmodel.cpp
               src/volume.cpp
               src/commonmetadata.cpp
               src/jnovelclubconnector.cpp
               src/volumelistmodel.cpp
               src/partlistmodel.cpp
               src/parts.cpp
               src/pageview.cpp
               src/commonmetadatasortfilterproxymodel.cpp
               src/user.cpp
               src/readingmode.cpp
               src/json_helper.cpp
               )

target_link_libraries(JNovelReader Qt5::Core Qt5::Gui Qt5::Network
    Qt5::Qml Qt5::QuickControls2 Qt5::Concurrent Qt5::WebView Boost::boost ${CMAKE_THREAD_LIBS_INIT})

feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
