import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Window 2.2
import QtQml 2.2

import QtQuick 2.2
import QtWebView 1.1

import "qrc:/qml/custom"

ApplicationWindow  {
    id: mainWindow
    visible: true
    //color: palette.base

    title: (lNReadingStack.depth !== 3) ? "J-Novel Club Reader" : novelClubConnector.lastFetchedPart.title

    Component.onCompleted: {
        novelClubConnector.checkIfLoggedIn();
        if (Qt.platform.os === "android") {
            width = Screen.desktopAvailableWidth;
            height = Screen.desktopAvailableHeight;
        } else {
            width = 1200;
            height = 600;
        }
        setX(Screen.width / 2 - width / 2);
        setY(Screen.height / 2 - height / 2);
    }

    Component {
        id: lNSeriesView
        SeriesView {showLNs: true}
    }

    Component {
        id: mangaSeriesView
        SeriesView {showLNs: false}
    }

    Component {
        id: partView
        PartReader {visible: lNReadingStack.visible}
    }

    Loader {
        id: latestPartsLoader
        active: false
        source: "qrc:/qml/custom/LatestReleasesDrawer.qml"
    }

    Loader {
        id: mangaWebViewLoader
        active: false
        sourceComponent: mangaWebView
        property string mangaUrl
    }

    Component {
        id: mangaWebView
        WebView {
            id: mangaViewer
            url: mangaWebViewLoader.mangaUrl
            onLoadingChanged: {
                if (loadRequest === WebView.LoadSucceededStatus	) {
                    runJavaScript("document.cookie = %1".arg(novelClubConnector.createAccessCookieString()));
                }
            }
            onUrlChanged: {
                console.log(url);
                let urlstring = "" + url;
                if (urlstring === "about:home" || !urlstring.includes("/mc/")) {
                    mangaReadingStack.pop();
                    mangaWebViewLoader.active = false;
                    if (Qt.platform === "android")
                        mainWindow.showNormal();
                } else {
                    mangaViewer.forceActiveFocus();
                    runJavaScript("document.querySelector(\"body\").addEventListener(\"keydown\", function(evt) { if (evt.key === \"Escape\" || evt.key === \"Esc\") {window.location.href = \"about:home\";} });");
                    if (Qt.platform === "android")
                        mainWindow.showFullScreen();
                }
            }
        }
    }

    SwipeView {
        id: mainView
        interactive: false
        focus: true
        height: bar.visible ? parent.height - bar.height : parent.height
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        currentIndex: bar.currentIndex

        ReadingStack {
            id: lNReadingStack
            showLNs: true
            visible: SwipeView.isCurrentItem
        }

        ReadingStack {
            id: mangaReadingStack
            showLNs: false
            visible: SwipeView.isCurrentItem

            Component.onCompleted: {
                novelClubConnector.fetchMangaSeriesList();
            }
        }

        LatestReleases {
            id: latestReleases
            visible: SwipeView.isCurrentItem
        }

        User {
            id: user
            visible: SwipeView.isCurrentItem
        }
    }

    TabBar {
        id: bar
        width: parent.width
        visible: !lNReadingStack.readerOpen
        TabButton {
            id: seriesOveriew
            text: "Series Overview"
            width: Math.max(implicitWidth, bar.width / mainView.count)
        }

        TabButton {
            text: "Manga"
            width: Math.max(implicitWidth, bar.width / mainView.count)
        }

        TabButton {
            text: "Latest Releases"
            width: Math.max(implicitWidth, bar.width / mainView.count)
        }

        TabButton {
            text: "Your Account"
            width: Math.max(implicitWidth, bar.width / mainView.count)
        }
    }

    ThemeUtil {
        id: themeUtil
    }
}
