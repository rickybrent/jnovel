import QtQuick 2.0

Rectangle
{
    property string heading: "You forgot to set the heading"
    height: headerText.contentHeight * 1.3
    width: parent.width
    border.color: "black"
    color: "blue"
    border.width: 1
    z: 3
    Text {id: reference; visible: false}
    Text {
        id: headerText
        font.pointSize: reference.font.pointSize*2
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        height: parent.height
        width: parent.width
        color: "#FFE"
        text: heading
    }
}
