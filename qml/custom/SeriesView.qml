import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import "qrc:/qml/js/misc.js" as Utils

FocusScope {
    property bool showLNs: true
    property var readingStack: showLNs ? lNReadingStack : mangaReadingStack
    property var partsModel: showLNs ? LNPartsModel : MangaPartsModel
    RowLayout {

        width: parent.width
        height: parent.height

        Item {
            Layout.preferredWidth: sBookView.visible ? parent.width / 3 : parent.width
            Layout.minimumWidth: parent.width /10
            Layout.preferredHeight: parent.height
            TitleListView {
                id: allVolumeTitles
                smartphoneMode: !currentVolume.visible
                KeyNavigation.tab: currentVolume
                heading: "Select Volume"
                titleModel: showLNs ? LNVolumeModel : MangaVolumeModel
                height:partsListView.visible ? parent.height - partsListView.height : parent.height
                width: parent.width

                onMyIndexChanged: {
                    titleModel.sourceModel.selectPartList(i);
                }
            }

            SequentialAnimation {
                id: refreshAnimation

                NumberAnimation {
                    target: allVolumeTitles
                    property: "opacity"
                    from: 1
                    to: 0
                    duration: 500
                    easing.type: Easing.InOutQuad
                }


                NumberAnimation {
                    target: allVolumeTitles
                    property: "opacity"
                    from: 0
                    to: 1
                    duration: 500
                    easing.type: Easing.OutInQuad
                }
            }

            Connections {
                target: allVolumeTitles
                onRefreshRequest: {
                    novelClubConnector.refreshVolumes();
                    refreshAnimation.restart();
                }
            }

            ListView {
                id: partsListView
                clip: true
                visible: !currentVolume.visible
                ScrollBar.vertical: ScrollBar {
                    policy: ScrollBar.AsNeeded
                }
                headerPositioning: ListView.OverlayHeader
                boundsBehavior: Flickable.StopAtBounds

                anchors.top:  allVolumeTitles.bottom
                width: parent.width
                height: parent.height * 2 / 5
                model: partsModel

                header: ListHeader {heading: "Select Part"}

                delegate: Button {
                    // we force a reevaluation of the text property after the reader returns from the partviewer by making it depend on readingStack.readerOpen
                    // thus an updaet is triggered when the user returns to the screen
                    text:  "Read Part " + (index+1) +  " (" + (readingStack.readerOpen, library.getPercentRead(partID)*100).toFixed(2) + "%) " +
                           (Utils.isPartAccessible(novelClubConnector.loggedIn, isExpired, isPreview) ? "" : "\ue98f")
                    font.family: "icomoon"
                    enabled: Utils.isPartAccessible(novelClubConnector.loggedIn, isExpired, isPreview)
                    width: parent.width
                    height: textQuery.height*2
                    ToolTip.visible: hovered
                    ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                    ToolTip.text: enabled ? ("Open " + text) : ("Expired")
                    onClicked: {
                        novelClubConnector.fetchPart(adapter, library.getPercentRead(partID));
                        readingStack.push(partView);
                    }
                }
            }
        }

        ColumnLayout {
            id: currentVolume
            Layout.fillHeight: true

            Layout.preferredHeight: parent.height
            Layout.preferredWidth: parent.width*2/3
            Layout.fillWidth: true
            visible: parent.width*2/3 >= sBookView.atLeastThisMuch  && parent.height >= sBookView.minimalRequiredHeight

            BookView {
                id: sBookView
                KeyNavigation.tab: partGridView
                curIt: allVolumeTitles.currentItem
                Layout.preferredWidth: currentVolume.width
                Layout.minimumWidth: sBookView.atLeastThisMuch
                Layout.minimumHeight: sBookView.minimalRequiredHeight
                Layout.preferredHeight: sBookView.minimalRequiredHeight
            }

            TextMetrics  {
                id: textQuery
                font.family: "icomoon"
                text: ">>Part 10 (100.00%) \ue98f<<"
            }

            Button {
                Layout.fillWidth: true
                text: "Back to Series Overview"
                onClicked: readingStack.pop();
            }

            GridView {
                id: partGridView
                KeyNavigation.tab: allVolumeTitles
                clip: true
                ScrollBar.vertical: ScrollBar {
                    policy: ScrollBar.AsNeeded
                }
                keyNavigationEnabled: false
                keyNavigationWraps: true
                Layout.preferredWidth: parent.width
                Layout.minimumHeight: cellHeight*2
                Layout.preferredHeight: cellHeight*3
                model: partsModel
                cellHeight: Math.min(textQuery.height * 3, 50);
                cellWidth: textQuery.width * 1.1

                delegate: Button {
                    property string partTitle: title // make attached model property accessible
                    property string partSlug: titleSlug
                    text: showLNs ? "Part " + (index+1) +  " (" + (readingStack.readerOpen, library.getPercentRead(partID)*100).toFixed(2) + "%) " +
                           (Utils.isPartAccessible(novelClubConnector.loggedIn, isExpired, isPreview) ? "" : "\ue98f") : partTitle
                    enabled: Utils.isPartAccessible(novelClubConnector.loggedIn, isExpired, isPreview)
                    font.family: "icomoon"
                    width: textQuery.width
                    //height: 35
                    ToolTip.visible: hovered
                    ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                    ToolTip.text: enabled ? ("Open " + text) : ("Expired")
                    onClicked: {
                        // TODO: this showuld also work with the list view
                        if (showLNs) {
                            novelClubConnector.fetchPart(adapter, library.getPercentRead(partID));
                            readingStack.push(partView);
                        } else {
                            partGridView.currentIndex = index;
                            const newurl = "https://j-novel.club/mc/" + partGridView.currentItem.partSlug;
                            mangaWebViewLoader.mangaUrl = newurl;
                            mangaWebViewLoader.active = true;
                            readingStack.push(mangaWebViewLoader);
                        }
                    }
                }
            }
        }
    }
}
