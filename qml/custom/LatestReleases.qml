import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import QtQuick.Controls.Material 2.12

import "qrc:/qml/js/misc.js" as Utils

FocusScope {
    id: latestPartsDrawer

    property bool smartphoneMode: latestPartsList.width <= 546 //magic constant, TODO: find a better way to determine the threshold

    function fetchLatest() {
        busy.running = true;
        novelClubConnector.fetchLatestReleasedParts()
    }

    BusyIndicator {
        id: busy
        anchors.centerIn: latestPartsList
        height: latestPartsList.height
        width: latestPartsList.width
        running: false
    }

    ListView {
        id: latestPartsList
        height: latestPartsDrawer.height - menu.height
        anchors.top: parent.top
        width: parent.width

        clip: true

        ScrollBar.vertical: ScrollBar {
            policy: ScrollBar.AlwaysOn
        }

        model: latestReleasedParts


        headerPositioning: ListView.OverlayHeader

        header: Rectangle {
            height: headerText.contentHeight * 1.3
            width: latestPartsDrawer.width
            border.color: "black"
            color: "blue"
            border.width: 1
            z: 3
            Text {
                id: headerText
                color: "white"
                text: "Latest Releases"
            }
        }

        delegate:
        Rectangle {
            property bool partAccessible: Utils.isPartAccessible(novelClubConnector.loggedIn, isExpired, isPreview)
            height: contentRow.height
            width: latestPartsDrawer.width
            color: index % 2 == 0 ? mainWindow.palette.base : mainWindow.palette.alternateBase
            property variant myData: model
            Row {
                id: contentRow
                height: Math.max(partThumbnail.height, titleLabel.contentHeight)
                Image {
                    id: partThumbnail
                    source: thumbnail
                    smooth: true
                    //sourceSize.width:  100
                    sourceSize.height: 120
                    width:  120
                    height: sourceSize.height
                    fillMode: Image.PreserveAspectFit
                }
                Text {
                    id: titleLabel
                    text: title + (partAccessible  ? "" : " \ue98f")
                    font.family: "icomoon"
                    width: latestPartsDrawer.width - partThumbnail.width
                    wrapMode: Text.Wrap
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: index === latestPartsList.currentIndex ? mainWindow.palette.highlightedText : mainWindow.palette.text
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    latestPartsList.currentIndex = index;
                    if (smartphoneMode)
                        openPartButton.clicked();
                }
                onDoubleClicked: {
                    latestPartsList.currentIndex = index;
                    openPartButton.clicked();
                }
            }
        }

        boundsBehavior: Flickable.DragOverBounds
        boundsMovement: Flickable.StopAtBounds

        property int oldOvershoot: 0
        property int maxOvershoot: 80
        property real triggerFactor: 0.75
        property bool fromTop: verticalOvershoot <= 0

        onVerticalOvershootChanged: {
            console.log(verticalOvershoot);
            if (verticalOvershoot === 0 && oldOvershoot > triggerFactor * maxOvershoot) {
                oldOvershoot = 0;
                console.log("hit");
                if (fromTop) {
                    // refresh everything
                    const numItems = latestPartsList.count;
                    busy.running = true;
                    novelClubConnector.fetchLatestReleasedParts(-2, numItems);
                } else {
                    // fetch more parts
                    fetchLatest();
                }
            } else if (Math.abs(verticalOvershoot) > oldOvershoot) {
                oldOvershoot = Math.abs(verticalOvershoot);
            }
        }

        onFromTopChanged: {
            if (fromTop) {
                refreshIndictor.anchors.bottom = undefined;
                refreshIndictor.anchors.top = latestPartsList.top;
            }
            else {
                refreshIndictor.anchors.top = undefined;
                refreshIndictor.anchors.bottom = latestPartsList.bottom;
            }
        }

        Item {
            id: refreshIndictor
            property int size: parent.height/7
            property int currentPos: Math.min(Math.abs(latestPartsList.verticalOvershoot), latestPartsList.maxOvershoot)
            visible: latestPartsList.verticalOvershoot !== 0
            width: size
            height: size
            anchors.topMargin: latestPartsList.fromTop ? currentPos : 0
            anchors.bottomMargin: !latestPartsList.fromTop ? currentPos : 0
            anchors.horizontalCenter: parent.horizontalCenter

            ProgressCircle {
                anchors.centerIn: parent
                opacity: 0.5 + Math.min(1, (refreshIndictor.currentPos / (latestPartsList.triggerFactor * latestPartsList.maxOvershoot)))*0.5
                size: parent.size
                colorCircle: Material.accent
                arcBegin: 0
                arcEnd: Math.max(40, 360*(parent.currentPos / latestPartsList.maxOvershoot))
                lineWidth: 10
                animationDuration: 20
                rotation: (refreshIndictor.currentPos / latestPartsList.maxOvershoot)  * 360
            }
        }

        highlight: Rectangle {
            color: mainWindow.palette.highlight
            opacity: 0.5
            z: 2
        }

        highlightMoveDuration: 0
    }

    RowLayout {
        id: menu
        anchors.top: latestPartsList.bottom
        Button {
            Layout.preferredWidth: latestPartsDrawer.width/2 - 10
            text: "Fetch more"

            onClicked: fetchLatest();
        }

        Button {
            id: openPartButton
            property bool canClickButton: false
            Layout.preferredWidth: latestPartsDrawer.width/2 - 10
            text: "Open Part"
            enabled: (openPartButton.canClickButton && latestPartsList.currentItem !== null && latestPartsList.currentItem.partAccessible)

            onClicked: {
                if (!latestPartsList.currentItem.partAccessible) {
                    return;
                }
                openPartButton.canClickButton = false;
                var index = latestReleasedParts.index(latestPartsList.currentIndex, 0);
                var part = latestReleasedParts.data(index, /*AdapterRole=*/261);
                novelClubConnector.fetchPart(part);
                readingStack.pop({item: null}); // clear the stack
                bar.currentIndex = 0; // focus tab
                readingStack.push([partView]);
            }

            Connections {
                target: latestReleasedParts
                onRowsInserted: {
                    openPartButton.canClickButton = true;
                }
            }
        }
    }

    Component.onCompleted: {
        fetchLatest();
    }

    Connections {
        target: novelClubConnector
        onFetchedLatestReleasedParts: {
            busy.running = false;
        }
    }
}
