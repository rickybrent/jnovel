import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import Qt.labs.settings 1.0

import club.jnovel.unfofficial 1.0

FocusScope {
    id: userPage
    Text {
        id: defaultFontProvider
        font.family: "Sans Serif"
        visible: false
        enabled: false
    }

    Column {
        width: parent.width
        height: parent.height

        Rectangle {
            id: loginStatusMessage
            height: message.height
            width: parent.width
            color: novelClubConnector.loginStatus === 3 ? "green" : (novelClubConnector.loginStatus === 1 ? "yellow" : "red")

            function status2message(status) {
                switch(status) {
                case 0:
                    return "You are not logged in. Only a limited amount of novels is accessible";
                case 2:
                    return "Login failed. Please check your username and password";
                case 3:
                    return "You are logged in";
                case 1:
                    return "Logging in…";
                }
            }

            Label {
                id: message
                width: parent.width
                height: implicitHeight*2
                text: loginStatusMessage.status2message(novelClubConnector.loginStatus)
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        ColumnLayout {
            id: loginFields
            visible: height > 0
            height: (novelClubConnector.loginStatus !== 3) ? implicitHeight : 0
            width: parent.width
            clip: true
            Behavior on height {
                NumberAnimation { duration: 500 }
            }

            TextField {
                id: emailField
                Layout.preferredWidth: userPage.width
                placeholderText: "Please enter your email address"
                text: (settings.emailAddress === undefined) ? "" : settings.emailAddress
                Keys.onReturnPressed: passField.forceActiveFocus()
            }

            function attemptLogin() {
                settings.emailAddress = emailField.text;
                settings.password = passField.text; // WARNING: storing password unencrypted
                novelClubConnector.login(emailField.text, passField.text);
            }

            TextField {
                id: passField
                Layout.preferredWidth: userPage.width
                placeholderText: "Please enter your password"
                text: (settings.password === undefined) ? "" : settings.password
                echoMode: TextInput.Password
                Keys.onReturnPressed: loginFields.attemptLogin();
            }

            Button {
                id: loginButton
                Layout.preferredWidth: userPage.width
                text: novelClubConnector.loggedIn ? "Logged in" : "Login"
                enabled: passField.text !== "" && emailField.text !== "" && !novelClubConnector.loggedIn && novelClubConnector.loginStatus !== 3
                onClicked: {
                    loginFields.attemptLogin();
                }
                BusyIndicator {
                    id: loggingInIndicator
                    anchors.fill: parent
                    width: parent.width
                    height: parent.height
                    running: novelClubConnector.loginStatus === 1;
                }
            }
        }

        Column {
            width: parent.width
            //height: previewText.contentHeight*1.1

            Settings {
                id: settings
                property alias font: defaultFontProvider.font
                property string colorScheme
                property string password
                property string emailAddress
                property int useVolumeButtons

                onFontChanged: {
                    fontComboBox.updateCurrentFont();
                    fontSizeField.text = settings.font.pointSize;
                }

                Component.onCompleted: {
                    if (colorScheme === undefined || colorScheme === "")
                        colorScheme = "SEPIA";
                    if (font === undefined || font === "")
                        font = Qt.font("Literata");
                }
            }

            Column {
                width: parent.width
                spacing: 10
                Row {
                    height: fontComboBox.height
                    width: parent.width
                    spacing: 10
                    Label {
                        id: fontNameLabel
                        text: "Current font is:"
                        verticalAlignment: Text.AlignVCenter
                        width: implicitWidth
                        height: fontComboBox.height
                    }
                    ComboBox {
                        id: fontComboBox
                        width: parent.width - fontNameLabel.width - 2*parent.spacing
                        model: Qt.fontFamilies()
                        function updateCurrentFont() {
                            for (var i=0; i<= model.length; ++i) {
                                if(model[i] === settings.font.family) {
                                    currentIndex = i;
                                    break;
                                }
                            }
                        }
                        onCurrentIndexChanged: {
                            if (model.length === 0)
                                return;
                            var oldFont = settings.font;
                            if (oldFont === undefined)
                                return;
                            oldFont.family = model[currentIndex];
                            settings.font = oldFont;
                        }
                    }
                }
                Row {
                    height: fontSizeField.height
                    width: parent.width
                    spacing: 10
                    Label {
                        id: fontSizeLabel
                        text: "Current font size:"
                        width: implicitWidth
                        height: fontSizeField.height
                        verticalAlignment: Text.AlignVCenter
                    }
                    TextInput {
                        id: fontSizeField
                        height: implicitHeight*2
                        width: parent.width - fontSizeLabel.width - 2*parent.spacing
                        text: settings.font !== undefined ? settings.font.pointSize : fontSizeLabel.font.pointSize
                        verticalAlignment: Text.AlignVCenter
                        onEditingFinished: {
                            var oldFont = settings.font
                            if (oldFont === undefined)
                                oldFont = fontSizeLabel.font
                            oldFont.pointSize = parseInt(fontSizeField.text);
                            settings.font = oldFont
                        }
                    }
                }
            }

            Row {
                height: readingModeComboBox.implicitHeight
                width: parent.width
                spacing: 10
                Label {
                    id: colorschemeLabel
                    text: "Current color scheme:"
                    width: implicitWidth
                    height: readingModeComboBox.height
                    verticalAlignment: Text.AlignVCenter
                }

                ComboBox {
                    id: readingModeComboBox
                    height: implicitHeight
                    width: parent.width - colorschemeLabel.width - 2*parent.spacing
                    // TODO: avoid duplication
                    function rgba(r,g,b,a) {return Qt.rgba(r/255,g/255,b/255,a);}
                    function bgColor() {
                        switch(currentIndex) {
                        case 0:
                        default:
                            return rgba(244, 236, 216, 1);
                        case 1:
                            return rgba(51,51,51, 1);
                        case 2:
                            return rgba(255,255,255, 1);
                        case 3:
                            return rgba(0,0,0, 1)
                        }
                    }

                    function fgColor() {
                        switch(currentIndex) {
                        case 0:
                        default:
                            return rgba(91, 70, 54, 1);
                        case 1:
                            return rgba(238,238,238, 1);
                        case 2:
                            return rgba(51,51,51, 1);
                        case 3:
                            return rgba(252,252,252, 1)
                        }
                    }

                    model: ["SEPIA", "DARK", "LIGHT", "BLACK"]
                    onActivated: {
                        settings.colorScheme = model[currentIndex];
                        themeUtil.update(model[currentIndex]);
                    }
                    currentIndex: themeUtil.scheme2id(settings.colorScheme)
                }
            }


            Label {
                id: previewLabel
                text: "Preview of how the text is displayed:"
                verticalAlignment: Text.AlignVCenter
            }

            Rectangle {
                width: parent.width
                height: previewText.implicitHeight
                color: readingModeComboBox.bgColor()
                clip: true
                Label {
                    anchors.centerIn: parent
                    id: previewText
                    text: "The quick brown fox jumps over the lazy dog\nPack my box with five dozen liquor jugs.\nSeveral fabulous dixieland jazz groups played with quick tempo."
                    color: readingModeComboBox.fgColor()
                    font: settings.font !== undefined ? settings.font : previewLabel.font
                }
                Behavior on height {
                    NumberAnimation { duration: 500 }
                }
            }
        }


        Column {
            width: parent.width
            Switch {
                id: useVolumeButtons
                text: "Turn Pages With Volume Buttons"
                checked: settings.useVolumeButtons
                onToggled: settings.useVolumeButtons = checked
            }
        }

        Button {
            id: libraryButton
            property bool showLib: false
            width: parent.width
            enabled: novelClubConnector.loginStatus === 3
            text: showLib ? "Hide Library" : "Show Library"
            Layout.preferredWidth: userPage.width
            onClicked: showLib = !showLib;
        }

        GridView {
            id: libraryGrid
            width: parent.width
            height: parent.height
            visible: novelClubConnector.loginStatus === 3 && libraryButton.showLib
            clip: true
            model: library
            cellWidth: 280
            cellHeight: 480
            delegate: Column {
                width: libraryGrid.cellWidth
                height: libraryGrid.cellHeight
                Image {
                    source: imageURL;
                    anchors.horizontalCenter: parent.horizontalCenter
                    sourceSize.width: 240
                    width: 240
                    height: 400
                    fillMode: Image.Pad
                }
                Label {
                    text: title
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: 240
                    wrapMode: Text.Wrap
                }

                Button {
                    text: "Download"
                    width: 240
                    enabled: false
                }
            }
        }
    }
}
