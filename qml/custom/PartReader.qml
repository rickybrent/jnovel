import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import club.jnovel.unfofficial 1.0

import QtQuick.Controls.Material 2.12
import Qt.labs.settings 1.0


FocusScope {
    property bool handleVolumeButtons: false
    Settings {
        property bool useVolumeButtons
        Component.onCompleted: {
            handleVolumeButtons = useVolumeButtons;
            if (handleVolumeButtons) {
                //qputenv("QT_ANDROID_VOLUME_KEYS", "1");
            }
        }
    }

    property bool nextPageEnabled: partPageView.pageNumber < partPageView.pageCount
    property bool prevPageEnabled: partPageView.pageNumber > 1
    property bool nextPartEnabled: true
    property bool prevPartEnabled: true

    property bool useLimitedView: (parent !== null) && parent.width < 940
    property bool useVeryLimitedView: (parent !== null) && parent.width < 600

    function gotoPrevPart() {
        var newPart = LNVolumeModel.sourceModel.getPrevPart(novelClubConnector.lastFetchedPart);
        novelClubConnector.fetchPart(newPart);
        partViewBusyIndicator.running = true;
        if (LNVolumeModel.sourceModel.getPrevPart(newPart).partID === "")
            prevPartEnabled = false;
        partPageView.pageNumber = partPageView.pageCount // hack to prevent buggy single page animation
        partPageView.pageNumber = 1;
        nextPartEnabled = true;
    }

    function gotoPrevPage() {
        var progress = partPageView.useTwoPages ? 2 : 1;
        if (prevPageEnabled) {
            partPageView.pageNumber -= progress;
        } else {
            partPageView.pageOffset = 0;
        }
    }

    function gotoNextPage() {
        var progress = (partPageView.useTwoPages ? 2 : 1);
        if (partPageView.pageNumber < partPageView.pageCount) {
            partPageView.pageNumber += progress;
        } else {
            partPageView.pageOffset = 0;
        }
    }

    function gotoNextPart() {
        var newPart = LNVolumeModel.sourceModel.getNextPart(novelClubConnector.lastFetchedPart);
        novelClubConnector.fetchPart(newPart);
        partViewBusyIndicator.running = true;
        if (LNVolumeModel.sourceModel.getNextPart(newPart).partID === "")
            nextPartEnabled = false;
        partPageView.pageNumber = partPageView.pageCount // hack to prevent buggy single page animation
        partPageView.pageNumber = 1;
        prevPartEnabled = true;
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        spacing: 0
        PartPageView {
            id: partPageView
            Layout.fillWidth: true
            Layout.fillHeight: true
            useTwoPages: !useLimitedView

            onParsingStarted: {
                partViewBusyIndicator.running = true;
            }

            onParsingEnded: {
                partViewBusyIndicator.running = false;
            }

            Component.onCompleted: {
                // "0" is the invalid ID
                if (LNVolumeModel.sourceModel.getPrevPart(novelClubConnector.lastFetchedPart).partID === "") {
                    prevPartEnabled = false;
                }
                if (LNVolumeModel.sourceModel.getNextPart(novelClubConnector.lastFetchedPart).partID === "")
                    nextPartEnabled = false;
                partPageView.keepScreenOn(true);
            }

            Component.onDestruction: {
                partPageView.keepScreenOn(false);
            }

            MouseArea {
                // swipe detection based on https://gist.github.com/kovrov/1742405
                property point origin
                property bool ready: false
                anchors.fill: parent
                propagateComposedEvents: true
                onPressed: {
                    drag.axis = Drag.XAndYAxis;
                    origin = Qt.point(mouse.x, mouse.y);
                }

                onPositionChanged: {
                    switch (drag.axis) {
                    case Drag.XAndYAxis:
                        if (Math.abs(mouse.x - origin.x) > 16) {
                            drag.axis = Drag.XAxis;
                        }
                        else if (Math.abs(mouse.y - origin.y) > 16) {
                            drag.axis = Drag.YAxis;
                        }
                        break;
                    case Drag.XAxis:
                        partPageView.pageOffset = mouse.x - origin.x;
                        break;
                    case Drag.YAxis:
                        break;
                    }

                }

                onDoubleClicked: {
                    partPageView.handleClick(mouse.x, mouse.y);
                }

                onReleased: {
                    switch (drag.axis) {
                    case Drag.XAndYAxis:
                        canceled(mouse)
                        break;
                    case Drag.XAxis:
                        var distance = mouse.x - origin.x;
                        if (Math.abs(distance) < partPageView.width/9) {
                            partPageView.resetPageOffset();
                            return;
                        }

                        if(distance < 0)
                            gotoNextPage();
                        else
                            gotoPrevPage();
                        break
                    case Drag.YAxis:
                        break;
                    }
                }
            }
            BusyIndicator {
                id: partViewBusyIndicator
                anchors.centerIn: partPageView
                running: true
                width: parent.width / 2
                height: parent.height / 2
            }
        }


        RowLayout {
            Layout.fillWidth: true
            id: uiButtons
            Layout.alignment: Qt.AlignVCenter|Qt.AlignHCenter

            Button {
                text: "Back to Part Overview"
                visible: !useLimitedView
                onClicked: readingStack.pop();
                Layout.alignment: Qt.AlignLeft
            }

            Button {
                id: prevPartButton
                font.family: "icomoon"
                text: "\uea18"
                enabled: prevPartEnabled
                onClicked: gotoPrevPart();
            }

            Button {
                id: prevButton
                visible: !useLimitedView
                font.family: "icomoon"
                text: "\uea44"
                enabled: prevPageEnabled
                onClicked: gotoPrevPage();
            }

            ComboBox {
                function range(i) {
                    var arr = new Array(i);
                    for (var j = 0; j<i; j++)
                        arr[j] = j+1;
                    return arr;
                }
                model: range(partPageView.pageCount)
                TextMetrics {id: reference; text: "999/999"}
                Layout.preferredWidth: reference.width +  2*leftPadding + 2*rightPadding
                displayText: "%1/%2".arg(partPageView.pageNumber).arg(partPageView.pageCount)
                onActivated: partPageView.pageNumber = index+1;
            }

            Button {
                visible: !useLimitedView
                text: partPageView.useTwoPages ? "Single Page" : "Book View \uD83D\uDCD6"
                onClicked: partPageView.useTwoPages = !partPageView.useTwoPages;
            }

            RowLayout {
                id: radioButtons
                visible: !useLimitedView
                ButtonGroup {
                    function scheme2button(scheme) {
                        switch(scheme) {
                        case ReadingNS.BLACK:
                            return blackButton;
                        case ReadingNS.DARK:
                            return darkButton;
                        case ReadingNS.SEPIA:
                            return sepiaButton;
                        case ReadingNS.LIGHT:
                            return lightButton;
                        }
                    }
                    id: readingMode
                    checkedButton: scheme2button(partPageView.colorScheme);
                }

                RadioButton {
                    id: lightButton
                    text: "LIGHT"
                    ButtonGroup.group: readingMode
                    onClicked: partPageView.setColorScheme(ReadingNS.LIGHT);
                }

                RadioButton {
                    id: sepiaButton
                    text: "SEPIA"
                    ButtonGroup.group: readingMode
                    onClicked: partPageView.setColorScheme(ReadingNS.SEPIA);
                }

                RadioButton {
                    id: darkButton
                    text: "DARK"
                    ButtonGroup.group: readingMode
                    onClicked: partPageView.setColorScheme(ReadingNS.DARK);

                }

                RadioButton {
                    id: blackButton
                    text: "BLACK"
                    ButtonGroup.group: readingMode
                    onClicked: partPageView.setColorScheme(ReadingNS.BLACK);
                }

            }

            ComboBox {
                visible: !radioButtons.visible && !useVeryLimitedView
                id: readingModeComboBox
                model: ListModel {
                    ListElement {key: "SEPIA"; value: 0}//0+ReadingNS.SEPIA}
                    ListElement {key: "DARK" ; value: 1}//0+ReadingNS.DARK}
                    ListElement {key: "LIGHT"; value: 2}//0+ReadingNS.Light}
                    ListElement {key: "BLACK"; value: 3}//0+ReadingNS.BLACK}
                    }
                textRole: "key"
                onActivated: partPageView.setColorScheme(currentIndex);
                currentIndex: themeUtil.scheme2id(partPageView.colorScheme)
            }

            Button {
                id: nextButton
                visible: !useLimitedView
                font.family: "icomoon"
                text: "\uea42"
                enabled: nextPageEnabled
                onClicked: gotoNextPage()
            }

            Button {
                id: nextPartButton
                font.family: "icomoon"
                text: "\uea19"
                enabled: nextPartEnabled
                onClicked: gotoNextPart();
            }

        }
    }

    Keys.onPressed: {
        console.log("HANDLE", handleVolumeButtons, event.key);
        let key = event.key
        if (handleVolumeButtons) {
            if (key === Qt.Key_VolumeDown) {
                key = Qt.Key_Right;
            } else if (key === Qt.Key_VolumeUp) {
                key = Qt.Key_Left;
            }
        }

        switch (key) {
            case Qt.Key_Right:
            case Qt.Key_Space:
                if (nextPageEnabled)
                    gotoNextPage();
                break;
            case Qt.Key_Left:
                if (prevPageEnabled)
                    gotoPrevPage();
                break;
            case Qt.Key_PageDown:
                if (prevPartEnabled)
                    gotoPrevPart();
                break;
            case Qt.Key_PageUp:
                if (nextPartEnabled)
                    gotoNextPart();
                break;
            default:
                event.accepted = false;
                return;
        }
        event.accepted = true;
    }

    Connections {
        target: novelClubConnector
        onPartFetched: {
            partPageView.setText(html, percentageRead)
        }
    }

    Connections {
        target: partPageView
        onPercentReadChanged: {
            novelClubConnector.updatePartCompletionStatus(novelClubConnector.lastFetchedPart, percentage);
            library.updatePercentRead(percentage, novelClubConnector.lastFetchedPart);
        }
        onColorSchemeChanged: {
            themeUtil.update(partPageView.colorScheme)
        }
    }
}
