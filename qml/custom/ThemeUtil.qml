import QtQuick 2.0
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0

import club.jnovel.unfofficial 1.0

Item {
    Settings {
        property string colorScheme
        Component.onCompleted: {
            update(colorScheme)
        }
    }
    SystemPalette { id: defaultPalette; colorGroup: SystemPalette.Active }
    property var darkPalette: ({
        window: "#323232",
        windowText: "white",
        base: "#323232",
        alternateBase: "#3C3C3C",
        text: "white",
        button: "#353535",
        buttonText: "white",
        brightText: "#0063A5",
        highlight: "#264F78",
        highlightedText: "white"
    })

    function scheme2id(scheme) {
        switch(scheme) {
        case "BLACK":
        case ReadingNS.BLACK:
            return 3;
        case "DARK":
        case ReadingNS.DARK:
            return 1;
        case "SEPIA":
        case ReadingNS.SEPIA:
            return 0;
        case "LIGHT":
        case ReadingNS.LIGHT:
            return 2;
        }
        return 0;
    }

    function update(colorScheme) {
        mainWindow.Material.background = undefined
        switch(colorScheme) {
        case "BLACK":
        case ReadingNS.BLACK:
            mainWindow.Material.background = "black"
        case "DARK":
        case ReadingNS.DARK:
            mainWindow.Material.theme = Material.Dark
            mainWindow.Universal.theme = Universal.Dark
            useDarkPalette(true);
            break;
        default:
            mainWindow.Material.theme = Material.Light
            mainWindow.Universal.theme = Universal.Light
            useDarkPalette(false);
        }
    }

    function useDarkPalette(dark) {
        for (const key in darkPalette) {
            mainWindow.palette[key] = dark ? darkPalette[key] : defaultPalette[key];
        }
    }
}
