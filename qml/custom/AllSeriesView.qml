import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

FocusScope {
    property var model
    property bool showLNs: true
    property var readingStack: showLNs ? lNReadingStack : mangaReadingStack
    RowLayout {

        width: parent.width
        height: parent.height

        TitleListView {
            id: allSeriesTitles
            smartphoneMode: !currentSeries.visible
            Layout.preferredWidth: bookView.visible ? parent.width / 3 : parent.width
            height: parent.height
            Layout.minimumWidth: parent.width /10
            titleModel: model
            implicitWidth: parent.width
            Layout.fillHeight: true
            heading: "Select Series"
        }

        ColumnLayout {
            id: currentSeries
            Layout.fillHeight: true
            Layout.preferredHeight: parent.height
            Layout.preferredWidth: parent.width*2/3
            Layout.fillWidth: true
            visible: parent.width*2/3 >= bookView.atLeastThisMuch && parent.height >= bookView.minimalRequiredHeight

            BookView {
                id: bookView
                Layout.preferredWidth: currentSeries.width
                Layout.minimumWidth: bookView.atLeastThisMuch
                Layout.minimumHeight: bookView.minimalRequiredHeight
                Layout.preferredHeight: bookView.minimalRequiredHeight
                curIt: allSeriesTitles.currentItem
            }
            Button {
                id: showVolumesButton
                Layout.fillWidth: true
                text: "Show Volumes"
                onClicked: openVolume();
            }
            Item {
                // spacer
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }

    function openVolume() {
        if (readingStack.depth > 1) {return;}
        if (showLNs) {
            LNVolumeModel.sourceModel.clear();
            novelClubConnector.fetchLNVolumesForID(allSeriesTitles.currentItem.myData.seriesID);
            readingStack.push(lNSeriesView);
        } else {
            console.log(allSeriesTitles.currentItem.myData.seriesID);
            novelClubConnector.fetchMangaVolumesForID(allSeriesTitles.currentItem.myData.seriesID);
            readingStack.push(mangaSeriesView);
        }

    }

    Connections {
        target: allSeriesTitles
        onDoubleClicked: {openVolume();}
    }

    Connections {
        target: allSeriesTitles
        onRefreshRequest: {
            novelClubConnector.fetchLNSeriesList();
            refreshAnimation.restart();
        }
    }

    SequentialAnimation {
        id: refreshAnimation

        NumberAnimation {
            target: allSeriesTitles
            property: "opacity"
            from: 1
            to: 0
            duration: 500
            easing.type: Easing.InOutQuad
        }


        NumberAnimation {
            target: allSeriesTitles
            property: "opacity"
            from: 0
            to: 1
            duration: 500
            easing.type: Easing.OutInQuad
        }
    }
}

