import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

FocusScope {
    id: readingStack
    function push(x) {stack.push(x);}
    function pop(x) {stack.pop(x);}
    property bool readerOpen: stack.currentItem instanceof PartReader
    property alias showLNs:  aSeriesView.showLNs

    StackView {
        id: stack
        anchors.fill: parent

        Keys.onPressed: {
            if ((event.key === Qt.Key_Escape) || (event.key === Qt.Key_Back)) {
                if (stack.depth > 1) {
                    stack.pop();
                    event.accepted = true;
                }
            }
        }

        initialItem: AllSeriesView {
            id: aSeriesView
            model: showLNs ? filterableLNSeriesModel : filterableMangaSeriesModel

        }

        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }
        popEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to:1
                duration: 200
            }
        }
        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to:0
                duration: 200
            }
        }

        onCurrentItemChanged: {
            if (currentItem) {
                currentItem.focus = true;
                currentItem.forceActiveFocus();
                if (currentItem instanceof PartReader) {
                    if (Qt.platform.os === "android")
                        mainWindow.showFullScreen();
                } else {
                    if (Qt.platform.os === "android")
                        mainWindow.showNormal();
                }
            }
        }

    }
}
