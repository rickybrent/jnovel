import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

FocusScope {
    id: bookView

    property Item curIt
    property int atLeastThisMuch: seriesImageID.width * 1.3
    property int minimalRequiredHeight: seriesImageID.height
    property alias imageWidth: seriesImageID.width

    Image {
            id: seriesImageID
            anchors.left: parent.left
            source: curIt ? (curIt.myData ? curIt.myData.imageURL : "") : ""
            width: 280
            height: 400
            smooth: true
            fillMode: Image.Pad
            sourceSize.width: 240
    }

    BusyIndicator {
        anchors.fill: seriesImageID
        running:  seriesImageID.status !== Image.Ready && seriesImageID.status !== Image.Error
    }


    ColumnLayout {
        width: parent.width - seriesImageID.width*1.1
        anchors.left: seriesImageID.right
        height: parent.height
        id: c

        property alias curIt: bookView.curIt


        Label {
            text: "<b>Author:</b> " + (c.curIt ? (c.curIt.myData ? c.curIt.myData.author : "") : "")
            Layout.preferredWidth: parent.width
            wrapMode: Text.Wrap
        }

        Label {
            text: "<b>Illustrator:</b> " + (c.curIt ? (c.curIt.myData ? c.curIt.myData.illustratorOrArtist : "") : "")
            Layout.preferredWidth: parent.width
            wrapMode: Text.Wrap
        }

        Label {
            text: "<b>Translator:</b> " + (c.curIt ? (c.curIt.myData ? c.curIt.myData.translator : "") : "")
            Layout.preferredWidth: parent.width
            wrapMode: Text.Wrap
        }

        Label {
            text: "<b>Editor:</b> " + (c.curIt ? (c.curIt.myData ? c.curIt.myData.editor : "") : "")
            Layout.preferredWidth: parent.width
            wrapMode: Text.Wrap
        }


        Label {
            text: "<b>Description:</b>"
            Layout.preferredWidth: parent.width
            wrapMode: Text.Wrap
        }

        ScrollView {
            focus: true
            Layout.preferredWidth: parent.width
            Layout.fillHeight: true
            ScrollBar.vertical.policy: ScrollBar.AsNeeded
            ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
            contentWidth: parent.width
            clip: true
            Label {
                id: descriptionText
                text: c.curIt ? (c.curIt.myData ? c.curIt.myData.description : "") : ""
                wrapMode: Text.Wrap
                textFormat: Text.PlainText
                width: parent.parent.width
            }
            Keys.onTabPressed: bookView.KeyNavigation.tab.forceActiveFocus()
            Keys.onBacktabPressed: bookView.KeyNavigation.backtab.forceActiveFocus()
        }
    }
}
