import QtQuick 2.0
import QtQuick 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import QtQuick.Controls.Material 2.12

FocusScope {
    id: titleListView

    property var titleModel
    property string heading
    property bool smartphoneMode: false

    property alias currentItem: titleList.currentItem
    property alias currentIndex: titleList.currentIndex
    property alias count: titleList.count

    signal myIndexChanged(int i)
    signal doubleClicked(int index)
    signal refreshRequest()

    BusyIndicator {
        width: parent.width/3
        height: parent.height
        running: titleList.count === 0
        anchors.centerIn: parent
    }

    ColumnLayout {

        width: parent.width
        height: parent.height

        TextField {
            id: filterField
            Layout.fillWidth: true
            placeholderText: "Filter series"
            onTextChanged: {
                titleModel.setFilterRegularExpression(filterField.text);
            }
        }

        ListView {
            focus: true

            Layout.fillWidth: true
            Layout.fillHeight: true

            ScrollBar.vertical: ScrollBar {
                policy: ScrollBar.AlwaysOn
            }
            id: titleList
            clip:  true
            model: titleModel

            Keys.onUpPressed: if (currentIndex > 0) currentIndex -= 1
            Keys.onDownPressed: if (currentIndex < count - 1) currentIndex += 1
            Keys.onReturnPressed: titleListView.doubleClicked(titleList.currentIndex) // TODO: rename doubleClicked signal
            Keys.onEnterPressed: titleListView.doubleClicked(titleList.currentIndex)

            Connections {
                target: titleList
                onCurrentItemChanged: {
                    myIndexChanged(currentIndex);
                }
            }

            onCountChanged: {
                // model has changed, make sure we select *something*
                currentIndex = 0;
            }

            headerPositioning: ListView.OverlayHeader

            header: ListHeader { heading: titleListView.heading }

            boundsBehavior: Flickable.DragOverBounds
            boundsMovement: Flickable.StopAtBounds

            property int oldOvershoot: 0
            property int maxOvershoot: 100
            property real triggerFactor: 0.75

            onVerticalOvershootChanged: {
                if (verticalOvershoot === 0 && oldOvershoot > triggerFactor * maxOvershoot) {
                    oldOvershoot = 0;
                    refreshRequest();
                } else if (-verticalOvershoot > oldOvershoot) {
                    oldOvershoot = -verticalOvershoot;
                }
            }

            Item {
                id: refreshIndictor
                property int size: parent.height/7
                property int currentPos: Math.min(-titleList.verticalOvershoot, titleList.maxOvershoot)
                visible: titleList.verticalOvershoot < 0
                width: size
                height: size
                anchors.top: parent.top
                anchors.topMargin: currentPos
                anchors.horizontalCenter: parent.horizontalCenter

                ProgressCircle {
                    anchors.centerIn: parent
                    opacity: 0.5 + Math.min(1, (refreshIndictor.currentPos / (titleList.triggerFactor * titleList.maxOvershoot)))*0.5
                    size: parent.size
                    colorCircle: Material.accent
                    arcBegin: 0
                    arcEnd: Math.max(40, 360*(parent.anchors.topMargin / 100))
                    lineWidth: 10
                    animationDuration: 20
                    rotation: (refreshIndictor.currentPos / titleList.maxOvershoot)  * 360
                }
            }

            highlightMoveDuration: 0

            delegate: RowLayout {
                property variant myData: model
                spacing: 0

                width: titleList.width
                //height: titleLabel.contentHeight * 1.1
                Rectangle {
                    Layout.minimumHeight: 120
                    Layout.minimumWidth: 80
                    Layout.preferredWidth: 80
                    color: textRect.color

                    Image {
                        source: previewImageURL
                        fillMode: Image.PreserveAspectFit
                        sourceSize.height: 120
                        smooth: true
                    }

                    MouseArea {
                        height: parent.height
                        width: titleList.width
                        onClicked: {
                            if (smartphoneMode) {
                                titleList.currentIndex = index;
                                titleListView.doubleClicked(index);
                            } else {
                                titleList.focus = true
                                titleList.currentIndex = index;
                            }
                        }

                        onDoubleClicked: {
                            titleList.currentIndex = index;
                            titleListView.doubleClicked(index);
                        }
                    }
                }

                Rectangle {
                    id: textRect
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    focus: true
                    color: index === currentIndex
                           ? mainWindow.palette.highlight
                           : (index % 2 == 0
                              ? mainWindow.palette.base
                              : mainWindow.palette.alternateBase)
                    Text {
                        id: titleLabel
                        text: title
                        width: parent.width
                        height: parent.height
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.Wrap
                        color: index === currentIndex ? mainWindow.palette.highlightedText : mainWindow.palette.text
                    }
                }
            }
        }

    }
}
