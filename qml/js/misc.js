.pragma library

function isPartAccessible(isLoggedIn, isExpired, isPreview) {
    return isLoggedIn ? (!isExpired || isPreview) : isPreview;
}
